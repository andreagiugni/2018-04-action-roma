<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name = "viewport" content = "width = 640">
<title>CV</title>

<?php 
		
$instructions = "Digita nome o società da cercare";
$notfoundtext = "Non è stato trovato alcun nome contenente ";

	if(isset($_GET['meeting']))
			$meeting = $_GET['meeting'];
		else
			$meeting = 31; 
			
			// QUESTA FUNZIONALITA' E' STATA CREATA PER IL MEETING DI SCENARI IMMOBILIARI. IL GUSCIO PASSA GIA'
			// L'INFORMAZIONE CORRETTA SUL MEETING. IMPOSTARE UN VALORE DI DEFAULT PERMETTE PERO' DI FARE
			// FUNZIONARE QUESTA FUNZIONALITA' ANCHE VIA WEB 
	
?>		
		
<link href="../data/<?php echo $meeting; ?>/whoswho/style.css" rel="stylesheet" type="text/css" media="all" />

</head>

<?php 
		
	
		if(isset($_GET['notfirsttime']))
			$notfirstime = 1;
		else {
			$notfirstime = 0;
		}
		
		if(isset($_GET['searchText'])) 
			$searchText = $_GET['searchText'];
	
		else 
		 	$searchText = "";

			
?>

<body style="padding:0;margin:0;">
<div class="overall">
  <table width="640" cellpadding=0 cellspacing="0" border="0">
   <tr>
      <td valign="middle" align="left" height="100" background="../data/<?php echo $meeting; ?>/whoswho/top.png">
      
      <form id="searchForm" action='index.php' method='get'>
      
      <input  id="notfirsttime" type="hidden" name="notfirsttime" / value="<?php echo $notfirstime; ?>"> 
      <input 
      
      style="position: absolute; z-index: 0; width: 570px; height: 50px; -webkit-border-radius: 0px; top: 20px; left: 20px; font-style:normal; font-family:Arial; font-size:30px;"
      
       type="text" id="searchText" name="searchText" onchange="searchForm.submit();" onclick="if(notfirsttime.value == 0) searchText.value = '';" value="<?php if($notfirstime) echo $searchText; else echo $instructions; ?>">
     
       
       </form></td>
    </tr>
   
    <tr><td valign="middle" align="left" height="345" class="top"></td></tr>
    
<?php
	
include("connect.php");

// VIENE UTILIZZATA PER EVIDENZIARE I TERMINI TROVATI NELL'ELENCO

function Highlight($needle, $haystack){ 
    $ind = stripos($haystack, $needle); 
    $len = strlen($needle); 
    if($ind !== false){ 
        return substr($haystack, 0, $ind) . "<font color='#660000'>" . substr($haystack, $ind, $len) . "</font>" . 
            highlight($needle, substr($haystack, $ind + $len)); 
    } else return $haystack; 
} 
 

function ExtendSearch($searchKeywords, $field) {
	
	$qall = "";
	
	for ($i = 0; $i < count($searchKeywords); $i++) {
			$qall .= "'%".$searchKeywords[$i]."%'";
			if($i != (count($searchKeywords) - 1))
				$qall .= " OR " .$field." LIKE ";
	}
	return $qall; 
}


function UpdateList($searchCriteria) {


global $db;
global $notfoundtext;

$searchKeywords = explode(" ", preg_replace( '/\s+/', ' ', trim($searchCriteria)));

	$searchQuery = "SELECT * FROM tbluserentity  WHERE (slastname LIKE ".ExtendSearch($searchKeywords, "slastname")."
											OR  sfirstname LIKE ".ExtendSearch($searchKeywords, "sfirstname")."
											OR  sistitution LIKE ".ExtendSearch($searchKeywords, "sistitution").")".
				
				
	// COMMENTARE LA RIGA SEGUENTE SE SI VOGLIONO VISUALIZZARE TUTTI GLI UTENTI DI TUTTI I PROFILI E NON SOLTANTO 
	// I PARTECIPANTI "NORMALI" CHE NON HANNO ALCUN PROFILO.
	// FATE QUESTO AD ESEMPIO SE VOLETE VERIFICARE L'ESISTENZA DELLE FOTO PER IL BADGE DEGLI UTENTI STAFF E GUEST
																				
											" AND sprofile = ''". 
	
	// DA QUI C'E' SEMPRE
										
											" ORDER BY slastname";

	$rows = $db->GetAll($searchQuery);
	
	if(count($rows) > 0) 
	foreach($rows as $row) {

		echo '<tr><td  valign="middle" align="left" height="100" class="borderbottom"><a href="./profile.php?p='
	 				.$row['pkiduser'].'" class="linkcurriculum" align="left">';
	  
	  	$slastname = $row['slastname'];
		
	  	for ($i = 0; $i < count($searchKeywords); $i++) 
			$slastname =  Highlight($searchKeywords[$i], $slastname);
		
		echo $slastname;
		  
		$sfirstname = $row['sfirstname'];
		
		for ($i = 0; $i < count($searchKeywords); $i++) 
    		$sfirstname =  Highlight($searchKeywords[$i], $sfirstname);
		
		echo ' '.$sfirstname;
		
		$sistitution = $row['sistitution'];
		
		for ($i = 0; $i < count($searchKeywords); $i++) 
    		$sistitution =  Highlight($searchKeywords[$i], $sistitution);
		
		if($sistitution != $row['sistitution'])
			echo '<font size=5><br>'. $sistitution."</font>";
		
		echo '</a></td></tr>';	
	
	}
	else
		echo '<tr><td class="linkcurriculum">'.$notfoundtext." <font color='#660000'>".$searchCriteria."</font></td></tr>";
	
}

	UpdateList($searchText);

?>

  <tr><td valign="middle" align="left" height="345"><font size=6>&nbsp;&nbsp;&nbsp;Elenco aggiornato alle ore 17 <br />&nbsp;&nbsp;&nbsp;di venerdì 9 settembre 2011</font></td></tr>


  <tr><td valign="middle" align="left" height="345" class="bottom"></td></tr>
  
</table>