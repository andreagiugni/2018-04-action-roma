
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1">

 <link href="screenCSS/style.css" rel="stylesheet" type="text/css" />
  <link href="screenCSS/font.css" rel="stylesheet" type="text/css" />
 <script src="jquery-1.4.2.js" type="text/javascript"></script>
<script>
	 function refreshA(){
		$.ajax({
				url: "screeniframe2.php", 
				data:{
					<?php
						if(isset($_GET["q"])&&isset($_GET["count"])){
							 echo "q: ".$_GET["q"].",";
							 echo "count: ".$_GET["count"].",";
						}
					?>
					meeting: <?php echo $_GET["meeting"] ?>,
					room: <?php echo $_GET["room"]?>					
					},
				success: function(data) { 
                    try{
                    
                        refreshBody(data);		
                   
                        refreshHeader();
                        
                    }
                    catch(e)
                    {
                        $("body").html("<div style='text-align:center;width:100%;margin-top:50%;'>NO DATA</div>");    
                    }
                    
                    
				}
			});		
 	 }
	 

	 
window.onload = function() {
	 refreshA();
	<?php if(!isset($_GET["q"])||!isset($_GET["count"])){ ?>
		window.setInterval(function(){ refreshA()},2000);
	<?php } ?>
	
	
	<?php if(isset($_GET["fastest"])&&$_GET["fastest"]!=''){ ?>
		setTimeout(function(){
			

			
			var counter=1;
			var score= parseInt($("#scoreContainer").text(),10);
			var gira= setInterval(function(){
					$("#scoreContainer").text((score+(counter++)));
					if(counter><?php echo $_GET["fastestBonus"]; ?>){
						clearTimeout(gira);
						$("#scoreContainer").addClass("pump");
					}
						
				},10);
				
				
				
			},3500);
	<?php }?>
    
    //refreshHeader();
    
    
}

function refreshHeader()
{
    
        var TESTI_DOMANDE = $("#questionContainer").attr("questtext").toString().split("|");
        $("#questionContainer").html(TESTI_DOMANDE[0]);
        
        if(TESTI_DOMANDE.length>1)
        {
          
            if(TESTI_DOMANDE[1].toUpperCase()=='ASSENSO')
            {
                $("body").css("background-image","url(screenCSS/screenBkg_assenso.png)");
                $("#typeQuestionContainer").text("ASSENSO ASSESSMENT");
                
            }
            else
            {
                
                var numdomanda =($("#questionContainer").attr("norder")=='0' ? '' : $("#questionContainer").attr("norder"));                
                $("#typeQuestionContainer").html("<b>MULTIPLE CHOICE</b> - DOMANDA " + numdomanda);
                $("body").css("background-image","url(screenCSS/screenBkg.png)");
            }
            
        }
        else
        {
            var numdomanda = ($("#questionContainer").attr("norder")=='0' ? '' : $("#questionContainer").attr("norder"));                
            $("#typeQuestionContainer").html("<b>MULTIPLE CHOICE</b> - DOMANDA " + numdomanda);
            $("body").css("background-image","url(screenCSS/screenBkg.png)");
        }
        //$("#questionContainer").fadeIn(500);
        //},500);
        
}
    
function refreshBody(data)
{
    $("body").html(data);
    
 
    var item_colors =[];
    item_colors[0] = 'one';
    item_colors[1] = 'two';
    item_colors[2] = 'three';
    item_colors[3] = 'four';
    item_colors[4] = 'five';
    
    var TESTI_DOMANDE = $("#questionContainer").attr("questtext").toString().split("|");
    if(TESTI_DOMANDE.length>1)
    {        
        
        
        
        if(TESTI_DOMANDE[1].toUpperCase()=='ASSENSO')
        {
            $(".alphaprogress").css("opacity","0");
            $(".answer").css("opacity","0");
            $(".barContainerBorder").css("opacity","0");                        
            
            $(".barContainerBorder_td").css("border","0");
            $(".percentContainer_td").css("background","transparent");
            
            //$(".answer_row").css("margin-top","-150px");
            
            var startValue = 5;
            var maxWidth = 664;
            var mediaPerc = [];
            var totalmedia = 0;
            var maxTopToAdd = 352;
            
            $(".answers_table tr").each(function(el){
                
                var curPercentage = $(this).find(".percentContainer").first().text().replace("%","");

                if(curPercentage>0)
                {                
                    mediaPerc[mediaPerc.length] = startValue * (curPercentage/100); 
                    totalmedia += mediaPerc[mediaPerc.length-1];
                }
                                
                newSize =  (maxWidth/100)*curPercentage;
                
                $(this).prepend("<td class='assensus_prog_container'><div class='assensus_prog " + item_colors[startValue-1] + "' style='width:" + newSize + "px'></div></td>")
                $(this).prepend("<td class='assensus_val " + item_colors[startValue-1] + "'>"+ startValue +"</td>")
                startValue--;
                
            });
            
            MediaCalcolata = eval(totalmedia);

            
            top_to_add = 206 + (5 - MediaCalcolata) * (560 -206) / 4; 
            
            $("#answersContainer").prepend("<div class='lblassenso'>TOTALMENTE D'ACCORDO</div>");
            $("#answersContainer").append("<div class='lblassensono'>NON D'ACCORDO</div>");
            $("<div class='imageMediana'></div>").insertAfter("#answersContainer");
            
            var strTop =eval(top_to_add) + "px"
            
            if(MediaCalcolata>0)
            {
				//setTimeout(function(){
			
					$(".imageMediana").animate({top:strTop},2000);
					//$(".imageMediana").css("top",strTop);
				//},3000);
                
            }
            
        }
        else
        {
            $(".alphaprogress").css("opacity","1");
            $(".answer").css("opacity","1");
            $(".barContainerBorder").css("opacity","1");
        }
    
    }

}
    
</script>
</head>
<body >

<div id="argumentContainer">RESULTS</div>

	<?php if(isset($_GET["fastest"])&&$_GET["fastest"]!=''){ 
			include("connect.php");
			global $db;
			$fastest = "select pkiduser, ISNULL(NULLIF(susername, ''), ('DEVICE '+CAST(fkiddevice AS VARCHAR))) as susername, ISNULL(CAST(szip AS INT) ,0) as score 
			            FROM tbluserentity	WHERE fkiddevice = ".$_GET["fastest"];
			$fastestDevice = $db->GetRow($fastest);
 			if($fastestDevice){
				echo '<div class="mask"><div class="fastestContainer">
							<div class="fastest">'.$fastestDevice["susername"].'</div>
							<div class="fastestScore" >
								<table width="100%" cellpadding="0" cellspacing="0" style="   border-collapse: collapse;">
									<tr>
										<td id="scoreContainer" align="right" width="100%">'.($fastestDevice["score"]- $_GET["fastestBonus"] ).'</td>
										<td>&nbsp;P</td>
									</tr>
								</table>
							</div>
						</div></div>';
			}
	}

	?>
</body>
</html>