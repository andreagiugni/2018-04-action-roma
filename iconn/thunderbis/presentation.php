<?php

$SHOW_SELECTORS = '1';
 
$MEETING = 0;
if(isset($_GET['meeting']))
	$MEETING = $_GET['meeting'];
	

$ROOM = 0;
if(isset($_GET['room']))
	$ROOM = $_GET['room'];
	

$QUESTIONLANGUAGE = 0;
if(isset($_GET['questionlanguage']))
	$QUESTIONLANGUAGE = $_GET['questionlanguage'];

$QUESTIONGROUP = 0;
if(isset($_GET['questionGroup']))
	$QUESTIONGROUP = $_GET['questionGroup'];
	
	
include("header.php");

echo "<hr/>";


?>


<style type = "text/css" >
	#listContainer *{-webkit-user-select:none;}
	#listContainer{ padding:0px;text-align: center; position:relative;}
    #listContainer li{ border-radius: 10px;text-align: left; list-style:none; clear:both; background:#FFC; display: inline-block; white-space:nowrap;margin:1px; border:1px solid #333}
    #listContainer li div{ float:left; display: inline-block;}
    .nameContainer{margin: 10px;}
    .sNum{margin: 10px; width: 20px; text-align: right;}
    .close{margin: 10px;}
    .textAreaContainer{min-width: 590px;min-height: 170px;}
    .radioContainer{margin: 10px;}
    #listContainer li.odd{ background:#CFC}
	#listContainer textarea{-webkit-user-select:auto;}
	#listContainer .nameContainerInput{-webkit-user-select:auto; width:160px}
 </style>
 
 
<script>

	function riordinaLista(){
		$('#listContainer li').removeClass('odd').each(
			function(index){  
				$(this).addClass(((index +1)%2==1?'odd':''));
				$('.sNum',$(this)).first().text(index +1)
			}
		);
	}
	
	
	function openQuestion(uId,file){
		$("textarea",$(document.getElementById(uId+file)).parent()).hide();
		$(document.getElementById(uId+file)).show();// per evitare caratteri speciali
	}
	
//	[{"fkIdQuestionGroup":"QUESTIONGROUP","folderName":"presentazioni+'.$_GET["presentationFolder"].'"},
//	   [{"nSlideNumber":"","sSlideType":"","fkIdQuestion":"","sFilename":"","questionBody":"",isOpen:0}]
//	]

	var fkIdQuestionGroup=<?php echo $QUESTIONGROUP; ?>;
	function send(){
		if(confirm("Confirm?")){
			/*Creo l'oggetto JSON che descrive la presentazione*/
			var jResult=[];
			$("#listContainer").attr("folderName");
			var detailes={"fkIdQuestionGroup":fkIdQuestionGroup,"folderName":$("#listContainer").attr("folderName")};
			jResult.push(detailes);
			var slides=[];
			var qCounter=1;
			$("#listContainer li").each(function(){
				var slide={};
				
				slide["nSlideNumber"]=$(".sNum",this).text();
				var sType=$('input[type=radio]:checked', this).val();
				slide["sSlideType"]=$('input[type=radio]:checked', this).val();
				slide["sFilename"]=$(this).attr('fileName');
			
				if(sType=="question"){
					slide["fkIdQuestion"]=qCounter++;
					//alert($('input[type=radio]:checked', this)[0].getAttribute("open"));
					if($('input[type=radio]:checked', this)[0].getAttribute("open")=="1"){
							slide["questionBody"]=$('textarea.openQ', this).val();
							slide["isOpen"]=1;
						}else{
							slide["questionBody"]=$('textarea.multQ', this).val();
							slide["isOpen"]=0;						
					}
	
				}
				slides.push(slide);
			});
			jResult.push(slides);
			
			
			
			/* Invio l'oggetto */
			$.ajax({
						type: "post",
						url: "presentationFillEngine.php",
						headers: { "cache-control": "no-cache" },
						contentType: "application/json",
	//				    dataType: 'json',
						data: JSON.stringify(jResult),
						success: function(data) {    
							//alert(data);
							//$("#log").html(data);
							window.location.href=window.location.href;
						},
						error: function() {
							//alert("Server non raggiungibile");
						}
					});	
		}
	}
	
	
	// Create Base64 Object
	var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

</script> 
<?php

/* SELECT PRESENTATION FOLDER */
  		 $meetingFolder="../data/".$MEETING."/";
		 $folder= $meetingFolder."presentazioni/";
		 if (is_dir($folder)){
			 
			echo "<form id=\"actionform\" action='".$_SERVER['PHP_SELF']."' method='get'>".$MEETINGANDROOM_INCLUDE;
			echo '<ul class="pageitem">';
			echo '<li class="select">';
			echo '
			
			<select name="presentationFolder" id="presentationFolder" onChange="document.getElementById(\'actionform\').submit();">';
		
			echo '<option value="0">&nbsp;&nbsp;SELECT THE PRESENTATION FOLDER</option>';			 
			 
			 
			  if ($files = opendir($folder)){
				while ($pres = readdir($files)) {	
					if(is_dir($folder.$pres)&&$pres!="."&&$pres!=".."){
						$selected=(isset($_GET["presentationFolder"])&&$_GET["presentationFolder"]==$pres)?"selected":"";
						echo '<option value="'.$pres.'" '.$selected.'>&nbsp;&nbsp;'.$pres.'</option>';				
					}
				}
				closedir($folder);
			  }
			echo '</select>';
			echo '<span class="arrow"></span>';
			echo '</li></ul></form>';
				
				
			
			/* Cartella selezionata */
			if(isset($_GET["presentationFolder"])&&is_dir($folder.$_GET["presentationFolder"])){
			
			if(is_dir($folder.$_GET["presentationFolder"])){	
				$currentFolder=$folder.$_GET["presentationFolder"]."/";
		
		
			
				echo '<div style="position:relative; display:inline-block; clear:both"> <label for="collapseInput" style=" line-height: 32px; float: left; margin:7px;">Griglia &nbsp;</label><input style="float: left;" onchange="$(\'.collapse\').toggle();" id="collapseInput" type="checkbox"> </div>  ';
				
				echo '<ul id="listContainer" folderName="presentazioni/'.$_GET["presentationFolder"].'">';
				
//				echo '<table id="listContainer" width="100%" cellpadding="0" cellspacing="0">';
				if ($currentFolderFiles = opendir($currentFolder)){
					$counter =1;
					while ($file = readdir($currentFolderFiles)) {	
				
					if($file!="."&&$file!=".."&&(is_dir($currentFolder.$file)||preg_match('/.png/i',$file)||preg_match('/.jpg/i',$file)))	
						{
							$real=$file;
							//$file=base64_encode($real);
						echo '<li class="'.($counter%2?'odd':'').'" fileName="'.$real.'" >
								<div class="close" > <input type="button" value="X" onclick="$(this).parents(\'li\').remove(); riordinaLista()"/></div>
								<div class="sNum">'.$counter++.'</div>';
						$checked=0;
						if (preg_match('/.png/i',$real)||preg_match('/.jpg/i',$real)){
							echo '<div class="nameContainer">'.$real.'<br/><img class="collapse" src="'.$currentFolder.$real.'" width="160px" style="border:1px solid #666;"/> </div>';
							
						}else if(is_dir($currentFolder.$real)){
							$ff=explode(".",$real);
							$checked=1;
							echo '<div class="nameContainer folder">Template:<br/><input class="nameContainerInput" value="'.$ff[0].'/index.html"></div>';	
						}
//						else echo '<td>'.$file.'</td>';	
						echo '<div class="radioContainer collapse">
							<input type="radio" name="radio'.$file.'" value="image" id="image'.$file.'" '.($checked?'':'checked').' onclick="$(\'textarea\',$(this).parent().next()).hide();"/>   <label for="image'.$file.'">Image</label><br/><br />
							<input type="radio" name="radio'.$file.'" value="interactive" id="interactive'.$file.'" onclick="$(\'textarea\',$(this).parent().next()).hide();" /> <label for="interactive'.$file.'">Interactive</label><br/><br />
							<input type="radio" name="radio'.$file.'" value="question" open="1" id="questionOpen'.$file.'" onclick="openQuestion(\'openQ\',\''.$file.'\')" /> <label for="questionOpen'.$file.'">Question (open answer)</label><br/><br />
							<input type="radio" name="radio'.$file.'" value="question" open="0" id="questionMultiple'.$file.'" '.($checked?'checked':'').' onclick="openQuestion(\'multQ\',\''.$file.'\')" /> <label for="questionMultiple'.$file.'">Question (multiple choice)</label><br/>
						</div>
						
						<div class="textAreaContainer collapse">
							<textarea class="openQ" id="openQ'.$file.'" name="openQ'.$file.'" rows="10" cols="70" style="display:none">New question (open answer)</textarea>
							<textarea class="multQ" id="multQ'.$file.'" name="multQ'.$file.'" rows="10" cols="70"  style="display:'.($checked?'block':'none').'">New question (multiple choice)</textarea>
						</div>
						</li>';
					}
				}
				echo '</ul>';
			
				echo '<br/><br/><div style="text-align: center;"><input type="button" value="Send" onclick="send();" style=" padding: 10px 100px; font-size: 15px;"> </div>  ';
		
				
				closedir($currentFolder);
				}
			}else{
				echo "<br/><br/>*** NON SO COME HAI FATTO, MA NON TROVO PIU' LA CARTELLA ".$folder.$_GET["presentationFolder"]." ***<br/><br/>";				
				}
				
		}else{  
		
		
		/* Edit Current*/
			
			
			echo "*** Edit current: ";
			$qEdit="Select sFolderName from tblquestiongroups where pkidquestiongroup=".$QUESTIONGROUP;
			$sFolderName= $db->GetRow($qEdit);
			
			if($sFolderName){
				
				$currentFolder=$meetingFolder.$sFolderName["sFolderName"]."/";
				
				echo '<br /><div style="position:relative; display:inline-block; clear:both"> <label for="collapseInput" style=" line-height: 32px; float: left; margin:7px;">Griglia &nbsp;</label><input style="float: left;" onchange="$(\'.collapse\').toggle();" id="collapseInput" type="checkbox"> </div>  ';
				
				echo '<ul id="listContainer" folderName="'.$sFolderName["sFolderName"].'">';
				
				$qEditCurrentSlides="Select * from tbl_LILLY_SlidesList where fkIdPresentation=".$QUESTIONGROUP." order by nSlideNumber";
				$sCurrentSlides= $db->GetAll($qEditCurrentSlides);
				
				foreach($sCurrentSlides as $slide){	
					echo '<li class="'.($slide["nSlideNumber"]%2?'odd':'').'" fileName="'.$slide["sFilename"].'" >
							<div class="close" > <input type="button" value="X" onclick="$(this).parents(\'li\').remove(); riordinaLista()"/></div>
							<div class="sNum">'.$slide["nSlideNumber"].'</div>';
					$checked='';
					$qTextO='New question (open answer)';
					$qTextM='New question (multiple choice)';				
					
					if ($slide["sSlideType"]=='image'){
						echo '<div class="nameContainer">'.$slide["sFilename"].'<br/><img class="collapse" src="'.$currentFolder.$slide["sFilename"].'" width="160px" style="border:1px solid #666;"/> </div>';
						$checked='image';
					}else if($slide["sSlideType"]=='interactive'){
						echo '<div class="nameContainer">'.$slide["sFilename"].'</div>';
						$checked='interactive';
					}
					else if($slide["sSlideType"]=='question'){
						//echo '<div class="nameContainer folder">Template:<br/><input class="nameContainerInput" value="'.$slide["sFilename"].'/index.html"></div>';	
						echo '<div class="nameContainer">'.$slide["sFilename"].'<br/><img class="collapse" src="'.$currentFolder.$slide["sFilename"].'" width="160px" style="border:1px solid #666;"/> </div>';
						
						$qQuest="Select * from tblquestionevaluationentity where fkidquestiongroup=".$QUESTIONGROUP." and norder=".$slide["fkIdQuestion"];
						$rQuest= $db->GetRow($qQuest);
					
						if($rQuest){
							if($rQuest["bText"]==1){ //Open answer
								$checked='questionO';
								$qTextO=$rQuest["squestion"];
							}else{  // Multiple choice
								$checked='questionM';
								$qTextM=$rQuest["squestion"];
								
								
								$qAnsEvaluation="Select * from tblanswerevaluation where ngroup=".$rQuest["ngroupanswer"]." order by norder";
								$rAnswers= $db->GetAll($qAnsEvaluation);
								
								if($rAnswers){
									foreach($rAnswers as $rAnswer){	
										$qTextM.="&#xA;".(trim($rAnswer["sanswer"])!=''?$rAnswer["sanswer"]:'***** empy option *****');
									}
								}else{
									$qTextM.='*** ATTENTION, no option for this question ***';
									}
							}
						}else{
								$checked='questionO';
								$qTextO='*** ATTENTION, no related questions in database ***';
								$qTextM='*** ATTENTION, no related questions in database ***';
						}
					}
					$file=$slide["sFilename"];
					echo '<div class="radioContainer collapse">
						<input type="radio" name="radio'.$file.'" value="image" id="image'.$file.'" '.(($checked=="image")?'checked':'').' onclick="$(\'textarea\',$(this).parent().next()).hide();"/>   <label for="image'.$file.'">Image</label><br/><br />
						<input type="radio" name="radio'.$file.'" value="interactive" id="interactive'.$file.'" '.(($checked=='interactive')?'checked':'').' onclick="$(\'textarea\',$(this).parent().next()).hide();" /> <label for="interactive'.$file.'">Interactive</label><br/><br />
						<input type="radio" name="radio'.$file.'" value="question" open="1" id="questionOpen'.$file.'" '.(($checked=='questionO')?'checked':'').' onclick="openQuestion(\'openQ\',\''.$file.'\')" /> <label for="questionOpen'.$file.'">Question (open answer)</label><br/><br />
						<input type="radio" name="radio'.$file.'" value="question" open="0" id="questionMultiple'.$file.'" '.(($checked=='questionM')?'checked':'').' onclick="openQuestion(\'multQ\',\''.$file.'\')" /> <label for="questionMultiple'.$file.'">Question (multiple choice)</label><br/>
					</div>
					
					<div class="textAreaContainer collapse">
						<textarea class="openQ" id="openQ'.$file.'" name="openQ'.$file.'" rows="10" cols="70" style="display: '.($checked=='questionO'?'block':'none').'">'.$qTextO.'</textarea>
						<textarea class="multQ" id="multQ'.$file.'" name="multQ'.$file.'" rows="10" cols="70" style="display: '.($checked=='questionM'?'block':'none').'">'.$qTextM.'</textarea>
					</div>
					</li>';
					}
				}
				echo '</ul>';
			
				echo '<br/><br/><div style="text-align: center;"><input type="button" value="Send" onclick="send();" style=" padding: 10px 100px; font-size: 15px;"> </div>  ';
			
		}		
				
?>	
  <script src="javascript/jquery-sortable.min.js"></script> 
  <script>
  $(function() {
    $("#listContainer").sortable({stop:riordinaLista});
    $("#listContainer").disableSelection();
  });
  </script>


<div id="log"></div>
			
				
<?php					
	}else{
		echo "<br/><br/>*** Inserire le cartelle con le presentazioni in data/#evento/presentazioni/ ***<br/><br/>";
	}
?>