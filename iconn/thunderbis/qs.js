

	var qsParm = new Array();

	function qs() {

		var query = window.location.search.substring(1);
		var parms = query.split('&');

		for (var i=0; i<parms.length; i++) {
			var pos = parms[i].indexOf('=');
			if (pos > 0) {
				var key = parms[i].substring(0,pos);
				var val = parms[i].substring(pos+1);
				qsParm[key] = val;
			}
		}
	}

	qs();    
   
   function GetParamOrDefaultValue(id, defaultValue) {
		
		returnValue = qsParm[id];
		if(typeof returnValue == "undefined") 
			returnValue = defaultValue;
		
		return returnValue;
	}
	
	 function GetParamOrDefaultIntValue(id, defaultValue) {
		
		returnValue = qsParm[id];

		if(typeof returnValue == "undefined"||returnValue=="") 
			returnValue = defaultValue;
		else
			returnValue = parseInt(returnValue);
		return returnValue;
	}
	
	function encode(toEncode) {
			var unencoded = toEncode;
			return encodeURIComponent(unencoded);
	}
	
	function decode(toDecode) {
			//var obj = document.getElementById('dencoder');
			var encoded = toDecode;
			return decodeURIComponent(encoded.replace(/\+/g,  " "));
	}// JavaScript Document