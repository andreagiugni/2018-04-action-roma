
$(function(e) {
    var appCache = window.applicationCache;
    

	appCache.addEventListener('updateready', function() {      
        // console.log("ci sonooo")  ;
        //  if (appCache.status == appCache.UPDATEREADY) {
	       //  appCache.swapCache();
        //     // init();
        //     setTimeout(function(){
	       //      	parent.$('body').trigger('cacheupdated');
	            	
	       //      	// $("#log").fadeOut(function(){
	       //      	// 	$(this).remove();
	       //      	// });
	       //      	alert("cio")
		      //       // window.location.reload();
        //     	},2000); 
        //  }
        if (appCache.status == appCache.UPDATEREADY) {
			appCache.swapCache();
            writeLog("Updated successfully");
            setTimeout(function(){
                window.location.reload();
           },2000);
        }

    }, false);

    // An update was found. The browser is fetching resources.
    appCache.addEventListener('downloading', function(){
        if ($("#appCacheLog").length==0){
        	$("body").append("<div id='appCacheLog'>"+($("body.mini").length==0?"UPDATING CONTENT:<br/><br/>":$("body.mini").attr("presentationName")+' - ')+"<span></span></div>");
        }
    }, false);

    // The manifest returns 404 or 410, the download failed,
    // or the manifest changed while the download was in progress.
    appCache.addEventListener('error', function (e) {
        writeLog('Error: Cache failed to update!');
    }, false);

    // appCache.addEventListener('noupdate', function (e) {
    //     // init();
    // }, false);

    appCache.addEventListener('cached', function (e) {  //viene scatenato la prima volta nel caso in cui non ci sia niente in cache
        writeLog("Downloaded successfully");
        setTimeout(function(){
            window.location.reload();
       },2000);
    }, false);

    // Fired for each resource listed in the manifest as it is being fetched.
    appCache.addEventListener('progress', function(e){
		if (e.lengthComputable) {
            writeLog('Downloading... ' + Math.round(e.loaded / e.total * 100) + "%");
            if(e.loaded == e.total){
            	setTimeout(function(){
                    window.location.reload();
               },5000);
            }
        }
    }, false);

    function writeLog(msg) {
    	$("#appCacheLog span").text(msg);
    }
});

