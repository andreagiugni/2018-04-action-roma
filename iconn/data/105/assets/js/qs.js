//Usare la variabile $_GET["nome"] esattamente come in php
(function() {
	window.$_GET = {};
	var query = window.location.search.substring(1)||window.location.hash.substring(1);
	var parms = query.split('&');

	for (var i=0; i<parms.length; i++) {
		var pos = parms[i].indexOf('=');
		if (pos > 0) {
			var key = parms[i].substring(0,pos);
			var val = parms[i].substring(pos+1);
			window.$_GET[key] = decodeURIComponent(val);
		}
	}
})();