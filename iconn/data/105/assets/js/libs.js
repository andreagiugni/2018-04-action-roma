//_EVENT_CLICK_TAP_TOUCHSTART_ = "click";
_EVENT_CLICK_TAP_TOUCHSTART_ = "tap";
var log_check = false;
function initWS() {
    if (_SCREEN_) {
        //_WS_.subscribe('totalVotes', wsevent);
        _WS_.subscribe('screenResults_' + _ROOM_, wsevent);
        _WS_.subscribe('questions_' + _ROOM_, wsevent);
    } else {
        _WS_.subscribe('questions_' + _ROOM_, wsevent);
        _WS_.subscribe('flush_' + _ROOM_, wsevent);
        _WS_.subscribe('reload_' + _ROOM_, wsevent);
        _WS_.subscribe('chooseRoom_' + _ROOM_, wsevent);
        _WS_.subscribe('setRoom', wsevent);
        _WS_.subscribe('showSurvey_' + _ROOM_, wsevent);
        _WS_.subscribe('pushnotification_' + _ROOM_, wsevent);
    }
    _WS_.call(_ROOM_);
}

function wsevent(service, data) {
    //whoAmI();
    //alert(JSON.stringify(data));
   /* var command={"s":service,"d":data};
    if(JSON.stringify(command)!=JSON.stringify(lastCommand)){
        lastCommand=command;
        setTimeout(function() {
            switch (service) {
                case 'questions_' + _ROOM_:
                    $(".screen-section").hide();
                    showQuestion(data);
                    $("#question").show();
                    break;
                case 'screenResults_' + _ROOM_:
                    if (_SCREEN_) {
                        showResults(data);
                        $("#dynamicContent").show();
                    }
                    break;
                case 'reload_' + _ROOM_:                
                    $(".screen-section").hide();
                    $("#cover").show();
                    break;
                case 'flush_' + _ROOM_:
                    $(".screen-section").hide();
                    getTweets(fillTweetList);
                    $("#home").show();
                    break;
            }
        }, 10);
    } */
    setTimeout(function() {
        switch (service) {
            case 'questions_' + _ROOM_:
                if (!_SCREEN_) {
                    $(".screen-section").hide();
                    showQuestion(data);
                    $("#question").show();
                    console.log("question");
                }
                else{
                    console.log("passato");
                    showQuestionScreen(data);
                }
                break;
            case 'screenResults_' + _ROOM_:
                if (_SCREEN_) {
                    showResults(data);
                    $("#dynamicContent").show();
                    console.log("result");
                }
                break;
            case 'reload_' + _ROOM_:     
                if (!_SCREEN_) {           
                    $(".screen-section").hide();
                    $("#cover").show();
                    console.log("reload");
                }
                break;
            case 'flush_' + _ROOM_:
                if (!_SCREEN_) {
                    localStorage.clear();
                    $("#askpresentation").hide();
                    $(".screen-section").hide();
                    getTweets(fillTweetList);
                    $("#home").show();
                }
                break;
            case 'chooseRoom_' + _ROOM_:
                
                if (!_SCREEN_) {
                    $(".screen-section").hide();
                    var rooms = data["rooms"];
                    var res = "";
                    /*for (var i = 0; i < rooms.length; i++) {
                        if(rooms[i]["pkidroom"]==180 || rooms[i]["pkidroom"]==181)
                        {
                            res+='<div class="room color-white bg-yellow text-center font-avenir-heavy" fkIdRoom="'+rooms[i]["pkidroom"]+'">'+rooms[i]["sname"]+'</div>';
                        }
                        else
                        {
                            res+='<div class="room color-white bg-blue text-center font-avenir-heavy" fkIdRoom="'+rooms[i]["pkidroom"]+'">'+rooms[i]["sname"]+'</div>';
                        }
                        
                    } */
                    res+='<div class="room color-white bg-yellow text-center font-avenir-heavy" fkIdRoom="181">Sala 1</div>';
                    res+='<div class="room color-white bg-blue text-center font-avenir-heavy" fkIdRoom="182">Sala 2</div>';                   
                    $("#choose-room .content-internal .btn-box").html(res);
                    $("#choose-room").show();
                    $(".room").on("tap", function() {
                        logInRoom($(this).attr("fkIdRoom"));
                    });
                }
                    break;
            case 'setRoom':
                logInRoom(data["room"]);
                break;
        }
    }, 10);
}

//function beep() {
//    var myAudio = document.getElementsByTagName('audio')[0];
//            //if (myAudio.paused)
//    if(myAudio)
//        myAudio.play();
//            //else
//            //    myAudio.pause();
//}

function logInRoom(room){
    $.ajax({
            type: 'POST',
            url: "http://" + location.host + "/iconn/phpservices/setMyRoom.php",
            data: {
                user: _USER_,
                destRoom: room 
            },
        success: function(data) {
            location.href = "http://" + location.host + "/iconn/kpad.php?noClearCache=1&uid=" + _UID_;
            }
        });

}

function updateTotalVotesCounter(bWsData) {
    var w = (bWsData["totalVotes"] / _Total_Devices_) * 500; //500 è la width massima
    $("#totalVotes_q" + bWsData["q"] + "_vs" + bWsData['vs']).css("width", w);
}

function goScore(tot, speed) {
    var curr = parseInt($("#scoreResult span").text(), 10);

    if (curr < tot) {
        $("#scoreResult span").text(curr + (tot / 50));
        setTimeout(function() {
            goScore(tot);
        }, speed);
    } else {
        $("#scoreResult span").text(tot);
    }
}

function setName(name) {
    $.ajax({
        type: 'POST',
        url: "http://" + location.host + "/iconn/phpservices/setTeamName.php",
        data: {
            device: $_GET["device"],
            nickName: name
        },
        success: function(msg) {
            if (name == msg) {
                $("#selectTeam").hide();
                whoAmI();
            } else
                alert(msg)
        }
    });
}


function whoAmI() {
    $.ajax({
        type: 'POST',
        url: "http://" + location.host + "/iconn/phpservices/getTeamByDevice.php",
        dataType: 'json',
        data: {
            device: $_GET["device"]
        },
        success: function(data) {
            //alert($_GET["device"]);
            team = data["susername"];
            score = Number(data["szip"]);
            $("body").attr("class", team);
            $("#teamName").text(team);
            $("#score span").text(score);
        }
    });
}

function touchHandler(event) {
    var touch = event;
    var x = (event.type == "touchend" || event.type == "touchcancel") ? 0 : touch.originalEvent.touches[0].pageX;
    var y = (event.type == "touchend" || event.type == "touchcancel") ? 0 : touch.originalEvent.touches[0].pageY;

    var simulatedEvent = document.createEvent("MouseEvent");
    simulatedEvent.initMouseEvent({
            touchstart: "mousedown",
            touchmove: "mousemove",
            touchend: "mouseup",
            touchcancel: "mouseup"
        }[event.type], true, true, window, 0,
        0, 0, x, y, false,
        false, false, false, 0, null);
    touch.target.dispatchEvent(simulatedEvent);
    event.preventDefault();
}
