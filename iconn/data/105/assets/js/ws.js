var _WS_;
var contError = 0;
$(function(){
    ab.connect("ws://10.220.220.10:8080", connect, disconnect, options);
});
var options = {
    retryDelay: 100,
    maxRetries: 99999
}
var connect = function(newSession){
    console.log("connesso");
    contError = 0;
    _WS_ = newSession;
    initWS();
    $("#conn-status").css("background-color", "green");
   
};
var disconnect = function(error){
    console.log("disconnesso");
    if(navigator.onLine){
        $("#conn-status").css("background-color", "yellow");
        if(error == 3){
            /*setTimeout(function(){
                location.reload();
            }, 1000);*/
            
        }
        if(error == 5){
            /*setTimeout(function(){
                location.reload();
            }, 1000);*/
        }
    }
    else
    {
        $("#conn-status").css("background-color", "red");
    }
    _WS_ = null;
    
};