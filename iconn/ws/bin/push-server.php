<?php 
	require dirname(__DIR__) . '/vendor/autoload.php';
	ini_set('display_errors', 1);
    $loop   = React\EventLoop\Factory::create();
    $pusher = new WsService\Pusher();//$db
	$lastEntry='';

    // Listen for the web server to make a ZeroMQ push after an ajax request
    $context = new React\ZMQ\Context($loop);
    $pull = $context->getSocket(ZMQ::SOCKET_PULL);
    $pull->bind('tcp://127.0.0.1:5555'); // Binding to 127.0.0.1 means the only client that can connect is itself
    $pull->on('message', array($pusher, 'onServiceCall'));
	/*$pull->on('message', 
			function($entry) {
				global $pusher;
				global $db;
				$entryData = json_decode($entry, true);
 				if(!isset($entryData['saveAsLastCommand'])||$entryData['saveAsLastCommand']){
					$db->Execute("DELETE from tblSocketNotification WHERE fkIdMeeting = ".$entryData['meeting']." AND fkIdRoom =".$entryData['room']." INSERT into tblSocketNotification VALUES ('".base64_encode($entry)."',".$entryData['meeting'].",".$entryData['room'].",'".$entry."')"); //".base64_decode($entry)."
				}
				
				$pusher->onServiceCall($entry);
            }
	);*/

    // Set up our WebSocket server for clients wanting real-time updates
    $webSock = new React\Socket\Server($loop);
    $webSock->listen(8080, '0.0.0.0'); // Binding to 0.0.0.0 means remotes can connect
	$webServer = new Ratchet\Server\IoServer(
        new Ratchet\Http\HttpServer(
            new Ratchet\WebSocket\WsServer(
                new Ratchet\Wamp\WampServer(
                    $pusher
                )
            )
        ),
        $webSock
    );

    $loop->run();
