<?php 
	require dirname(__DIR__) . '/vendor/autoload.php';
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
    $loop   = React\EventLoop\Factory::create();
    $pusher = new MyApp\Pusher;
	$lastEntry='';

    // Listen for the web server to make a ZeroMQ push after an ajax request
    $context = new React\ZMQ\Context($loop);
    $pull = $context->getSocket(ZMQ::SOCKET_PULL);
    $pull->bind('tcp://0.0.0.0:5555'); // Binding to 127.0.0.1 means the only client that can connect is itself
    $pull->on('message', array($pusher, 'onServiceCall'));

    // Set up our WebSocket server for clients wanting real-time updates
    $webSock = new React\Socket\Server($loop);
    $webSock->listen(8080, '0.0.0.0'); // Binding to 0.0.0.0 means remotes can connect
	$webServer = new Ratchet\Server\IoServer(
        new Ratchet\Http\HttpServer(
            new Ratchet\WebSocket\WsServer(
                $pusher
            )
        ),
        $webSock
    );

    $loop->run();