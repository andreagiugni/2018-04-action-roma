<?php
namespace MyApp;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;  
//use Ratchet\Wamp\WampServerInterface;
//use React\EventLoop\LoopInterface;
error_reporting(E_ALL);
ini_set('display_errors', 1);



	
class Pusher implements MessageComponentInterface {
    protected $clients;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn) {
        $this->clients->attach($conn);
        echo "\nEccolo: ".$conn-> remoteAddress." (#Clients: ".count($this->clients).")";
        $conn->send("welcome");
        
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        foreach ($this->clients as $client) {
            if ($from != $client) {
                $client->send($msg);
            }
        }
    }

    public function onServiceCall($entry) {
        global $lastEntry;
        $lastEntry=$entry;
        foreach ($this->clients as $client) $client->send($entry);
    }

    public function onClose(ConnectionInterface $conn) {
        $this->clients->detach($conn);
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        $conn->close();
    }
}
	