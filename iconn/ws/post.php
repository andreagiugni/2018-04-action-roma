<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	$entryData = array(
        'service' => 'questions'
      , 'title'    => 'testHello'
      , 'article'  => 'tuttoRego'
      , 'when'     => time()
    );

    $context = new ZMQContext();
    $socket = $context->getSocket(ZMQ::SOCKET_PUSH, 'dealer');
    $socket->connect("tcp://localhost:5555");

    sendWebSocket($entryData);