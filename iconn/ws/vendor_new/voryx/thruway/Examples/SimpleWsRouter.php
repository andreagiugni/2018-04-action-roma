<?php

require 'bootstrap.php';

use Thruway\Peer\Router;
use Thruway\Transport\RatchetTransportProvider;

$router = new Router();

$transportProvider = new RatchetTransportProvider("192.168.2.87", 9090);

$router->addTransportProvider($transportProvider);

$router->start();
