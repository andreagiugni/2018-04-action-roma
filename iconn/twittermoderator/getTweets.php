<?php
error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);

if($_GET['meeting'])
    $MEETING = $_GET['meeting'];
else
   exit("error id meeting"); 	 
if($_GET['room'])
    $ROOM = $_GET['room'];
else
   exit("error id room"); 	
   
if($_GET['questionGroup'])
    $SESSION = $_GET['questionGroup'];
else {
	 exit("error id session"); 	
}
if($_GET['tweetsession'])
    $TWEETSESSION = $_GET['tweetsession'];
else {
	$TWEETSESSION =0;
}
if($_GET['type'])
    $TYPE = $_GET['type'];
else {
	$TYPE ="";
}
if($_GET['alert'])
    $ALERT = $_GET['alert'];
else {
   $ALERT ="enable";
}

$QUESTIONLANGUAGE ="";   
if($_GET['questionlanguage'])
    $QUESTIONLANGUAGE = $_GET['questionlanguage'];
  
require_once("config.php");
require_once (constant('INCLUDE')."header.php"); 
$thunder=new Thunder($db,$CURRENT_LANGUAGE );

switch($QUESTIONLANGUAGE)
{
	case "it-IT":
		$alt_approve="Approva";
		$alt_edit="Modifica";
		$alt_delete="Sposta nel cestino";
		$alt_onscreen="Invia a schermo";
		$alt_delscreen="Rimuovi dalla proiezione";
		$alt_recupera="Recupera";
		$alt_toapprove="Sposta in da approvare";
	break;
	case "en-GB":
		$alt_approve="Approve";
		$alt_edit="Modify";
		$alt_delete="Move to trash";
		$alt_onscreen="Send to screen";
		$alt_recupera="Restore";
		$alt_delscreen="Delete from screen";
		$alt_toapprove="Move to approve";
	break;
	default:
		$alt_approve="Approve";
		$alt_edit="Modify";
		$alt_delete="Move to trash";
		$alt_onscreen="Send to screen";
		$alt_recupera="Restore";
		$alt_delscreen="Delete from screen";
		$alt_toapprove="Move to approve";
	break;
}

$html="";  							

if($TYPE=="TOAPPROVE")
{
	//Tutti i tweet da approvare
	$qry = sprintf("SELECT * FROM tbltweets WHERE fkidTweetingSession =%d AND bdeleted = 0 AND bApproved = 0 AND bMarked = 0 ORDER BY bShowed,pkidTweet DESC", $SESSION);
	$rows = $db->GetAll($qry);	
	
	if($rows){
        if($GLOBALS['BROWSER']=="mobile")
        {
            $html.="<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"data\">".PHP_EOL;
            $html.="<thead><tr><th>Messaggio</th><th colspan=\"3\">Azioni</th></tr></thead> <tbody>".PHP_EOL;
            
        }
        else
        {
		    $html.="<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" >".PHP_EOL;
         }   
		$idx=0;
		foreach($rows as $row) {
			
			if($idx%2==1)
				if($row['bShowed'])
				{
					$html.="<tr class=\"error\">".PHP_EOL;
				}
				else{
					$html.="<tr class=\"alternate-row\">".PHP_EOL;
				}
			else	
			{
				if($row['bShowed'])
				{
					$html.="<tr class=\"error\">".PHP_EOL;
				}
				else{
					$html.="<tr >".PHP_EOL;
				}
			}
			
			 if($GLOBALS['BROWSER']=="mobile")
			{
				if($row['bShowed'])
				{
				$html.="<td style=\"color:#f00;\">".$row['sbody']."</td>".PHP_EOL;
				}
				else
				{
					$html.="<td>".$row['sbody']."</td>".PHP_EOL;
				}
				$html.="<td style=\"width:24px;\"><a href=\"javascript:;\" class=\"invia\" title=\"$alt_approve\" id=\"approve_".$row['pkidTweet']."\"></a></td>".PHP_EOL;
				$html.="<td style=\"width:24px;\"><a href=\"javascript:;\" class=\"edit\" title=\"$alt_edit\" id=\"edit_".$row['pkidTweet']."\"></a></td>".PHP_EOL;
				$html.="<td style=\"width:24px;\"><a href=\"javascript:;\" class=\"totrash\" title=\"".$row['pkidTweet']."\"></a></td>".PHP_EOL;
			}
			else
			{
				$html.="<td>".$row['sbody']."</td>".PHP_EOL;
				$html.="<td style=\"width:24px;margin:0;padding:0\"><a href=\"javascript:;\" class=\"invia\" title=\"$alt_approve\" id=\"approve_".$row['pkidTweet']."\"></a></td>".PHP_EOL;
				$html.="<td style=\"width:24px;margin:0;padding:0\"><a href=\"javascript:;\" class=\"edit\" title=\"$alt_edit\" id=\"edit_".$row['pkidTweet']."\"></a></td>".PHP_EOL;
				$html.="<td style=\"width:24px;margin:0;padding:0\"><a href=\"javascript:;\" class=\"totrash\" title=\"".$row['pkidTweet']."\"></a></td>".PHP_EOL;
			}									
			
			$html.="</tr>".PHP_EOL;
			$idx++;
		}
		$html.="</table>".PHP_EOL;
		$html.="<script>
		$(\".totrash\").click(function(){  
		   	 var title = $(this).attr(\"title\");			
			 DeleteTweet(title);
				
		});";
        if($ALERT=="enable")
        {
                $html.="$(\".invia\").click(function(){  
                    var temp = $(this).attr(\"id\");            
                    var id=temp.split('_');
                    jConfirm('Are you sure?', 'APPROVE TWEET ID: '+id[1], function(r) {
                        if(r)
                        {
                             ApproveTweet(id[1]);
                        }
                    });                        
                });";
        }
        else
        {
             $html.="$(\".invia\").click(function(){  
                    var temp = $(this).attr(\"id\");            
                    var id=temp.split('_');                   
                    ApproveTweet(id[1]);
                });";
        }        
        $html.="$(\".edit\").click(function(){  
		   var temp = $(this).attr(\"id\");			
		   var id=temp.split('_');		
			editTweet(id[1]);				
		});
		</script>";
	}
}

if($TYPE=="APPROVED")
{
	//Tutti i tweet da approvare
	$qry = sprintf("SELECT * FROM tbltweets WHERE fkidTweetingSession =%d AND bdeleted = 0 AND bMarked = 0 AND bApproved = 1 ORDER BY bShowed,pkidTweet DESC", $SESSION);
	$rows = $db->GetAll($qry);	
	
	if($rows){
		if($GLOBALS['BROWSER']=="mobile")
        {
             $html.="<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"data\">".PHP_EOL;
             $html.="<thead><tr><th>Messaggio</th><th colspan=\"3\">Azioni</th></tr></thead> <tbody>".PHP_EOL;
        }
        else
        {
		    $html.="<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" >".PHP_EOL;
         }   
		$idx=0;
		foreach($rows as $row) {
			
			if($idx%2==1)
				if($row['bShowed'])
				{
					$html.="<tr class=\"error\">".PHP_EOL;
				}
				else{
					$html.="<tr class=\"alternate-row\">".PHP_EOL;
				}
			else	
			{
				if($row['bShowed'])
				{
					$html.="<tr class=\"error\">".PHP_EOL;
				}
				else{
					$html.="<tr >".PHP_EOL;
				}
			}
			 if($GLOBALS['BROWSER']=="mobile")
			{
				if($row['bShowed'])
				{
				$html.="<td style=\"color:#f00;\">".$row['sbody']."</td>".PHP_EOL;
				}
				else
				{
					$html.="<td>".$row['sbody']."</td>".PHP_EOL;
				}

				$html.="<td style=\"width:24px;\"><a href=\"javascript:;\" class=\"onscreen\" title=\"$alt_onscreen\" id=\"onscreen_".$row['pkidTweet']."\"></a></td>".PHP_EOL;
				$html.="<td style=\"width:24px;\"><a href=\"javascript:;\" class=\"disapprove\" title=\"$alt_toapprove\" id=\"toapprove_".$row['pkidTweet']."\"></a></td>".PHP_EOL;
            	$html.="<td style=\"width:24px;\"><a href=\"javascript:;\" class=\"edit\" title=\"$alt_edit\" id=\"edit_".$row['pkidTweet']."\"></a></td>".PHP_EOL;
			}
			else
			{
				$html.="<td>".$row['sbody']."</td>".PHP_EOL;		
				$html.="<td style=\"width:24px;margin:0;padding:0\"><a href=\"javascript:;\" class=\"onscreen\" title=\"$alt_onscreen\" id=\"onscreen_".$row['pkidTweet']."\"></a></td>".PHP_EOL;
				$html.="<td style=\"width:24px;margin:0;padding:0\"><a href=\"javascript:;\" class=\"disapprove\" title=\"$alt_toapprove\" id=\"toapprove_".$row['pkidTweet']."\"></a></td>".PHP_EOL;
           		$html.="<td style=\"width:24px;margin:0;padding:0\"><a href=\"javascript:;\" class=\"edit\" title=\"$alt_edit\" id=\"edit_".$row['pkidTweet']."\"></a></td>".PHP_EOL;
			}				
			
			
			$html.="</tr>".PHP_EOL;
			$idx++;
		}
		$html.="</table>".PHP_EOL;
		$html.="<script>".PHP_EOL;
        if($ALERT=="enable")
        {
                  $html.="$(\".onscreen\").click(function(){  		
	                    var temp = $(this).attr(\"id\");			
	                    var id=temp.split('_');
		                    jConfirm('Are you sure?', 'SEND TO SCREEN TWEET ID: '+id[1], function(r) {
			                    if(r)
			                    {
		 		                    sendtoscreenTweet(id[1]);
			                    }
		                    });						
                    });".PHP_EOL;
          }          
          else
          {
                  $html.="$(\".onscreen\").click(function(){          
                        var temp = $(this).attr(\"id\");            
                        var id=temp.split('_');
                         sendtoscreenTweet(id[1]);
                    });".PHP_EOL;
          }
         $html.="$(\".disapprove\").click(function(){  		
	                        var temp = $(this).attr(\"id\");			
	                        var id=temp.split('_');		                        
	                        disapproveTweet(id[1]);
                        });
</script>".PHP_EOL;
	}
}
if($TYPE=="ONSCREEN")
{
	//Tutti i tweet da approvare
	$qry = sprintf("SELECT * FROM tbltweets WHERE fkidTweetingSession =%d AND bdeleted = 0 AND bMarked = 1 ORDER BY pkidTweet DESC", $SESSION);

	$rows = $db->GetAll($qry);	
	
	if($rows){
        if($GLOBALS['BROWSER']=="mobile")
        {
            $html.="<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"data\">".PHP_EOL;
            $html.="<thead><tr><th>Messaggio</th><th colspan=\"2\">Azioni</th></tr></thead> <tbody>".PHP_EOL;
            
        }
        else
        {
		    $html.="<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" >".PHP_EOL;
         }   
		$idx=0;
		foreach($rows as $row) {
			
			if($idx%2==1)
				if($row['bShowed'])
				{
					$html.="<tr class=\"error\">".PHP_EOL;
				}
				else{
					$html.="<tr class=\"alternate-row\">".PHP_EOL;
				}
			else	
			{
				if($row['bShowed'])
				{
					$html.="<tr class=\"error\">".PHP_EOL;
				}
				else{
					$html.="<tr >".PHP_EOL;
				}
			}
			 if($GLOBALS['BROWSER']=="mobile")
			{
				if($row['bShowed'])
				{
				$html.="<td style=\"color:#f00;\">".$row['sbody']."</td>".PHP_EOL;
				}
				else
				{
					$html.="<td>".$row['sbody']."</td>".PHP_EOL;
				}	
				$html.="<td style=\"width:24px;\" ><a href=\"javascript:;\" class=\"delscreen\" title=\"$alt_onscreen\" id=\"delscreen_".$row['pkidTweet']."\"></a></td>".PHP_EOL;
				$html.="<td style=\"width:24px;\"><a href=\"javascript:;\" class=\"totrash\" title=\"$alt_delete\" id=\"totrash_".$row['pkidTweet']."\"></a></td>".PHP_EOL;
								
			}
			else
			{
				$html.="<td>".$row['sbody']."</td>".PHP_EOL;			
				$html.="<td style=\"width:24px;margin:0;padding:0\" ><a href=\"javascript:;\" class=\"delscreen\" title=\"$alt_onscreen\" id=\"delscreen_".$row['pkidTweet']."\"></a></td>".PHP_EOL;
				$html.="<td style=\"width:24px;margin:0;padding:0\"><a href=\"javascript:;\" class=\"totrash\" title=\"$alt_delete\" id=\"totrash_".$row['pkidTweet']."\"></a></td>".PHP_EOL;
			}
			
			
			$html.="</tr>".PHP_EOL;
			//$html.="</div>".PHP_EOL;
			$idx++;
		}
          if($GLOBALS['BROWSER']=="mobile") 
                $html.="</tbody>".PHP_EOL;
            
		$html.="</table>".PHP_EOL;
		$html.="<script>".PHP_EOL;
        if($ALERT=="enable")
        {
                    $html.="$(\".delscreen\").click(function(){  		
		                var temp = $(this).attr(\"id\");			
		                var id=temp.split('_');
		                jConfirm('Are you sure?', 'REMOVE FROM SCREEN TWEET ID: '+id[1], function(r) {
			                if(r)
			                {
		 		                removefromscreenTweet(id[1]);	
			                }
		                });		
	                });".PHP_EOL;
                     $html.="$(\".totrash\").click(function(){          
                        var temp = $(this).attr(\"id\");            
                        var id=temp.split('_');
                        jConfirm('Are you sure?', 'MOVE TO TRASH TWEET ID: '+id[1], function(r) {
                            if(r)
                            {
                                 DeleteTweet(id[1]);    
                            }
                        });        
                    });".PHP_EOL;     
                    
           }
           else
           {
                 $html.="$(\".delscreen\").click(function(){          
                        var temp = $(this).attr(\"id\");            
                        var id=temp.split('_');                       
                        removefromscreenTweet(id[1]);                            
                    });".PHP_EOL;
                 $html.="$(\".totrash\").click(function(){          
                        var temp = $(this).attr(\"id\");            
                        var id=temp.split('_');                     
                        DeleteTweet(id[1]);                              
                    });".PHP_EOL;   
           }
        $html.="</script>".PHP_EOL;
	}
}
if($TYPE=="TRASH")
{
	//Tutti i tweet da approvare
	$qry = sprintf("SELECT * FROM tbltweets WHERE fkidTweetingSession =%d AND bdeleted = 1 ORDER BY pkidTweet DESC", $SESSION);
	$rows = $db->GetAll($qry);	
	
	if($rows){
		if($GLOBALS['BROWSER']=="mobile")
        {
             $html.="<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"data\">".PHP_EOL;
             $html.="<thead><tr><th>Messaggio</th><th >Azioni</th></tr></thead> <tbody>".PHP_EOL;
        }
        else
        {
		    $html.="<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" >".PHP_EOL;
         }   
		$idx=0;
		foreach($rows as $row) {
			
			if($idx%2==1)
				if($row['bShowed'])
				{
					$html.="<tr class=\"error\">".PHP_EOL;
				}
				else{
					$html.="<tr class=\"alternate-row\">".PHP_EOL;
				}
			else	
			{
				if($row['bShowed'])
				{
					$html.="<tr class=\"error\">".PHP_EOL;
				}
				else{
					$html.="<tr >".PHP_EOL;
				}
			}		
			 if($GLOBALS['BROWSER']=="mobile")
			{
				$html.="<td >".$row['sbody']."</td>".PHP_EOL;		
				$html.="<td style=\"width:24px;\"><a href=\"javascript:;\" class=\"restore\" title=\"$alt_recupera\" id=\"trash_".$row['pkidTweet']."\"></a></td>".PHP_EOL;													
			}
			else
			{
				$html.="<td>".$row['sbody']."</td>".PHP_EOL;	
				$html.="<td style=\"width:24px;margin:0;padding:0\"><a href=\"javascript:;\" class=\"restore\" title=\"$alt_recupera\" id=\"trash_".$row['pkidTweet']."\"></a></td>".PHP_EOL;	
			}														
			$html.="</tr>".PHP_EOL;
			$idx++;
		}
		$html.="</table>".PHP_EOL;
		$html.="<script>
			
		$(\".restore\").click(function(){  
		    var temp = $(this).attr(\"id\");			
		    var id=temp.split('_');
			restoreTweet(id[1]);	
		});
		</script>";
	}
}


echo ($html);
?>
