<?php
	include("connect.php");
	
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
	
	$MEETING = $_POST['meeting'];
	$ROOM = $_POST['room'];
	$presentationName = $_POST['presentationName'];
	$nCurrentSlide = $_POST['nCurrentSlide'];
	$totalSlides = $_POST['totalSlides'];
	$refresh = $_POST['refresh'];




	$entryData = array( 
			'meeting' => $MEETING,
			'room' =>  $ROOM,
			'service' => 'changeSlide_'.$ROOM, //*** Importante ***
			'wsData' => array(
				"presentationName"=>$presentationName,
				"totalSlides"=>$totalSlides,
				"nCurrentSlide"=>$nCurrentSlide,
				"refresh"=>$refresh
				)
			);
	sendWebSocket($entryData);
    
?>