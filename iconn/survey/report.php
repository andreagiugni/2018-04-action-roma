<?php
	include("../adodb/adodb.inc.php");
    include("../include/connect.php");

	$DEVICE = $_GET['device'];
		
	function GetUserNameForDevice($id,$db) {
		$row = $db->GetRow("SELECT * FROM tbluserentity WHERE fkiddevice  = '".$id."'");		
		if($row)
			return $row['slastname']." ".$row['sfirstname'];
		else
			return "iPad ".$_GET['device'];
	}
	


	$idmeeting = 53;
	if(isset($_GET["meeting"])){
		$idmeeting = $_GET["meeting"];
	}
	
	$idroom = 81;
	if(isset($_GET["room"])){
		$idroom = $_GET["room"];
	}
	
	$idsession = 1;
	if(isset($_GET["sessionid"])){
		$idsession = $_GET["sessionid"];
	}
	
	$query = "";
	$query = "SELECT * FROM tblSurveyQuestions 
			WHERE fkIDSurveySession = ".  $idsession ."
			ORDER BY nOrdine ASC, pkIDSurveyQuestion asc";
	$questions = $db->GetAll($query);
	$domande = array_reverse($questions);

	function getMaxRispText($numero){
		if($numero > 0){
			return "(max ".$numero." risposte)";
		}
	}
	
	function getCountRisposte($db1,$answerid){
		$queryex = "SELECT count(pkIDUserAnswer) as conta
					FROM tblSurveyUserAnswers
					WHERE fkIDSurveyAnswer = ".  $answerid ."
					GROUP BY fkIDSurveyAnswer";
		$result = $db1->GetRow($queryex);
		return "<span class='countrisposte'>". ($result["conta"] + 0) ."</span> ";
	}
	
	function getAltroText($db1,$answerid){
		$queryex = "SELECT sTesto
					FROM tblSurveyUserAnswers
					WHERE fkIDSurveyAnswer = ".  $answerid ."";
		$result = $db1->GetAll($queryex);	
		
		return "<span class='countrisposte'>". array_map("stampaTesto", $result)."</span> ";
	}
	
	function stampaTesto($obj){
		return $obj["sTesto"].", ";
	}
	//print_r($slideObj);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Survey</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<script src="../js/libs/jquery-1.7.1.min.js" type="text/javascript"></script>
<!-- <script src="js/jquery-ui-1.8.17.custom.min.js" type="text/javascript"></script> -->
<style>
body {
	font-size:18px;
	font-family:Arial;
	padding:0px;
	margin:0px;
}
.wrapper{

}
.domande{
	padding:40px 40px 0px 40px;
	
}
.domanda{
	border:1px solid #959322;
}
.titolo{
	color:#FFF;
	background-color:#c3c282;
	padding:10px;
	border-bottom:#959322;
	font-size:26px;
}
.risposte{
	margin:10px 0px 20px 0px;	
}
.risposte ul, .risposte li{
	list-style:none;
	list-style-position:inside;
	margin:0px;
	padding:0px;
}
.risposte ul li input[type=checkbox], .risposte li input[type=radio] {
	width:40px;
	height:40px;

}
.risposte ul li label{
	line-height:44px;
	font-size:24px;
	list-style-type:none;
	vertical-align:top;
	
	padding:10px;
}
.risposte ul li input[type=radio]{
	line-height:82px;
	font-size:38px;
	height:40px;
	width:40px;
}
	
.risposte ul li input[type=checkbox]{
	line-height:82px;
	font-size:38px;
	height:40px;
	width:40px;
}
select{
	font-size:24px;
	padding:5px;
	width:894px;
  	margin-left:25px;

}
select option{
	padding:5px;
}

.inline-texbox{
	display:inline;
	vertical-align:top;
	padding:2px;
	font-size:20px;
	margin:5px;

}
input[type=submit]{
	line-height:30px;
	font-size:30px;
	height:40px;
	padding:5px 20px 5px 20px;
	text-transform:uppercase;
}

span.countrisposte{
	line-height:44px;
	font-size:24px;
	list-style-type:none;
	vertical-align:top;
	
	padding:10px;
	color:#DD0000;
}
</style>

<script>
$(document).ready(function(){
	$(".risposte input[type=checkbox]").click(function(){
		var numchecked = 0;
		var maxchecked = 0;
		numchecked = $(this).parent().parent().find(":checked").length;
		maxchecked = $(this).parent().parent().parent().find(".max-answers").val();
		if(maxchecked > 0){
				if( numchecked > maxchecked){
					$(this).removeAttr("checked");
					alert("Massimo " + maxchecked + " risposte");
				}
		}
	});
	
});
</script>
</head>
<body>
<form action="save.php" enctype="multipart/form-data" id="form1" name="form1" method="post">
<div class="header"><img src="../data/<?php echo $idmeeting ?>/survey/header.jpg" /></div>
<div class="wrapper">

	<?php while($question = array_pop($domande)) { ?>
    <?php 
		$query = "";
		$query = "SELECT * FROM tblSurveyAnswers 
				WHERE fkIDSurveyQuestion = " . $question["pkIDSurveyQuestion"] . "
				ORDER BY nOrdine ASC, pkIDSurveyAnswer asc ";
		$answers = $db->GetAll($query);		
		$risposte = array_reverse($answers);
	?>
	<div class="domande">
        <div class="domanda">
            <div class="titolo"><?php echo $question['sDomanda']." <span style='color:#000;'>".getMaxRispText($question["nMaxAnswers"])."</span>" ?></div>
            <div class="risposte">
            <?php switch($question["nTipo"]){ 
				
					case 1:;
            
					case 2: { ?>
                    	<ul> 
                        <?php while($answer = array_pop($risposte)) { ?>
                        	
                        	<li><?php echo getCountRisposte($db,$answer["pkIDSurveyAnswer"]) ?>
                            <label for="group_<?php echo $answer["fkIDSurveyQuestion"] ?>"><?php echo $answer["sTesto"] ?></label>
                            <?php 
								?>
                            </li>
						<?php } ?>                        
                        </ul>
                   
                                        
            <?php 	} break; ?>            

			<?php	case 3: { ?>
                    	<ul> 
                        <?php while($answer = array_pop($risposte)) { ?>
                        	<li><?php echo getCountRisposte($db,$answer["pkIDSurveyAnswer"]) ?>
                            <label for="opt_<?php echo $answer["pkIDSurveyAnswer"] ?>"><?php echo $answer["sTesto"] ?></label>
                            <?php if($answer["nTipoAnswer"]==2){?>
                            		
                            <?php }?>
                            </li>
						<?php } ?>                        
                        </ul>
                         <input type="hidden" class="max-answers" id="maxanswers<?php echo $question["pkIDSurveyQuestion"] ?>" value="<?php echo $question["nMaxAnswers"] ?>"></div>
            <?php 	} break; ?>            
			
   			<?php	case 4: { ?>
                    	<textarea id="text_<?php echo $answer["pkIDSurveyAnswer"] ?>" name="text_<?php echo $answer["pkIDSurveyAnswer"] ?>"></textarea>
            <?php 	} break; ?>   
                      
            <?php } ?>
            
            </div>        
		</div>
    </div>
    <?php } ?>
</div><br /><br /><div align="center"></div><br /><br />
<div class="footer"><img src="../data/<?php echo $idmeeting ?>/survey/footer.jpg" /></div><br /><br />
</form>
</body>
</html>