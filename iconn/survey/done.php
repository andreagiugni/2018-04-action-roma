<?php
		include("../adodb/adodb.inc.php");
    include("../include/connect.php");

	$DEVICE = $_GET['device'];
		
	function GetUserNameForDevice($id,$db) {
		$row = $db->GetRow("SELECT * FROM tbluserentity WHERE fkiddevice  = '".$id."'");		
		if($row)
			return $row['slastname']." ".$row['sfirstname'];
		else
			return "iPad ".$_GET['device'];
	}
	


	$idmeeting = 53;
	if(isset($_GET["meeting"])){
		$idmeeting = $_GET["meeting"];
	}
	
	$idroom = 81;
	if(isset($_GET["room"])){
		$idroom = $_GET["room"];
	}
	
	$idsession = 1;
	if(isset($_GET["sessionid"])){
		$idsession = $_GET["sessionid"];
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Survey</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<script src="../js/libs/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="../js/libs/jquery.validate.js" type="text/javascript"></script>
<!-- <script src="js/jquery-ui-1.8.17.custom.min.js" type="text/javascript"></script> -->
<style>
body {
	font-size:18px;
	font-family:Arial;
	padding:0px;
	margin:0px;
}
.wrapper{

}
.domande{
	padding:40px 40px 0px 40px;
	
}
.domanda{
	border:1px solid #959322;
}
.titolo{
	color:#FFF;
	background-color:#c3c282;
	padding:10px;
	border-bottom:#959322;
	font-size:26px;
}
.risposte{
	margin:10px 0px 20px 0px;	
}
.risposte ul, .risposte li{
	list-style:none;
	list-style-position:inside;
	margin:0px;
	padding:0px;
}
.risposte ul li input[type=checkbox], .risposte li input[type=radio] {
	width:40px;
	height:40px;

}
.risposte ul li label{
	line-height:44px;
	font-size:24px;
	list-style-type:none;
	vertical-align:top;
	
	padding:10px;
}
.risposte ul li input[type=radio]{
	line-height:82px;
	font-size:38px;
	height:40px;
	width:40px;
}
	
.risposte ul li input[type=checkbox]{
	line-height:82px;
	font-size:38px;
	height:40px;
	width:40px;
}
select{
	font-size:24px;
	padding:5px;
	width:894px;
  	margin-left:25px;

}
select option{
	padding:5px;
}

.inline-texbox{
	display:inline;
	vertical-align:top;
	padding:2px;
	font-size:20px;
	margin:5px;

}
input[type=submit]{
	line-height:30px;
	font-size:30px;
	height:40px;
	padding:5px 20px 5px 20px;
	text-transform:uppercase;
}

</style>


  <style>
  /*	label { float: left; font-family: Arial, Helvetica, sans-serif; font-size: small; }
	br { clear: both; }
	input { border: 1px solid black; margin-bottom: .5em;  }
	label.error {
		display:none;
		background: rgba(180,0,0,0.5);
		padding-left: 16px;
		margin-left: .3em;
	}
	label.valid {
		background: rgba(0,180,0,0.5);
		display: block;
		width: 16px;
		height: 16px;
	}
	*/
</style>
</head>
<body>

<div class="header"><img src="../data/<?php echo $idmeeting ?>/survey/header.jpg" /></div>
<div class="wrapper">
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<div align="center">Grazie per aver risposto alle domande.</div>
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</div>
<div class="footer"><img src="../data/<?php echo $idmeeting ?>/survey/footer.jpg" /></div><br />
</form>
</body>
</html>