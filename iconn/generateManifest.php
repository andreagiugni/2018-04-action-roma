<?php
	error_reporting(E_ALL);
	ini_set('display_errors', '1');

	if(isset($_GET["meeting"])){
		$this_page = basename($_SERVER['REQUEST_URI']);
		if (strpos($this_page, "?") !== false) {
			$tmpArr=explode("?", $this_page);
			$this_page = reset($tmpArr);
		}

		$now=date("Y-m-d H:i:s");
		$eventFolder="./data/".$_GET["meeting"]."/";

		$fname = "./data/".$_GET["meeting"]."/kpad.appcache";
		$fhandle = fopen($fname, 'w') or die("can't open file");
		$content = "CACHE MANIFEST\n#".$now."\n\n# Explicitly cached 'kPad 2.0 master'.\n\n# Resources for meeting: ".$_GET["meeting"]."\nCACHE:\n";

		function go($files,$folder){	
			global $this_page;
			global $content;
			global $counter;
			global $fname;
			global $eventFolder;
			
			while ($file = readdir($files)) { 
				if(
					$file!='.'
					&&$file!='..'
					&&$file!=$fname
					&&$file != $this_page
					&&$file[0] != '_'
					&&strpos($folder.$file, 'Thumbs') === false
					){
						if(is_dir($eventFolder.$folder.$file)){
							go(opendir("./".$eventFolder.$folder.$file."/"),  $folder.$file."/");
						}else {
							$content .= $folder.$file."\n";	
						}
				}
			}	
		}

		go(opendir($eventFolder),"");
		
		$content.="\n\nNETWORK:\n*";
		
		fwrite($fhandle, $content);	
		fclose($fhandle);

		echo nl2br($content);
	}else{
		echo "** ERROR: no 'meeting' specified in query string";
	}
?>