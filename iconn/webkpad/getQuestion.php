<?php
include("../phpservices/connect.php");

error_reporting(E_ALL);

ini_set('display_errors', '1');


function strictify ( $string ) {
	$fixed = html_entity_decode($string, ENT_QUOTES, 'UTF-8');
	return $fixed;
}

function translatecontent($id, $table, $lang) {
	
	global $db;
	
	$row = NULL;
	
	if($table == "questions") {
	
		$q22 =  "SELECT * FROM tbltranslations WHERE fkid  = '".$id."' AND stable = '".$table."' AND slanguage = '".$lang."'";
		//echo $q22;
		
		$row = $db->GetRow($q22);	
	}
	
	if($table == "answers") {
	
		$q33 =  "SELECT * FROM tbltranslations WHERE fkid  = '".$id."' AND stable = '".$table."' AND slanguage = '".$lang."'";
		
		//echo $q33;
		
		$row = $db->GetRow($q33);	
	}

	if($row) 
		return $row['stranslation'];
	else
		return NULL;
}


	if(isset($_GET['lang']))
		$QUESTIONLANGUAGE = $_GET['lang'];
	else
		$QUESTIONLANGUAGE = "";
	
	$q =$_GET["q"];
	$count = $_GET["count"];

	$rw = $db->GetRow("SELECT * FROM tblquestionevaluationentity WHERE pkidquestion = '".$q."'");
	$translation = translatecontent($q, "questions", $QUESTIONLANGUAGE);

	if($translation)  
		$rw['squestion'] = $translation;

	$testogruppo = "";

	if(isset($rw['squestion']) && $rw['squestion'] != "") 
		$out_domanda = "###".$rw['nmaxanswers']."###".$rw['norder']."###".$testogruppo."###".strictify($rw['squestion'])."###";
	else {
		$out_domanda = "###1###0###Errore###DOMANDA NON TROVATA"."###";
		continue;
	}

	if(isset($rw['ngroupanswer'])) {
		$qws = $db->GetAll("SELECT * FROM tblanswerevaluation WHERE ngroup = '".$rw['ngroupanswer']."' ORDER BY norder ASC");
	
		foreach($qws as $qw) {
		
		
		
		$translation = translatecontent($qw['pkidanswer'], "answers", $QUESTIONLANGUAGE);

			if($translation)  
				$qw['sanswer'] = $translation;
				
			$out_domanda .= $qw['pkidanswer']."###".strictify($qw['sanswer'])."###";
		}
	}

	echo $out_domanda;


?>