var debug = 0;

function ParseContent() {

	var xmlDoc = ParseHTTPResponse (httpRequest);  
        
	if (!xmlDoc)
        return;		
							
	var xmlRoot = xmlDoc.getElementsByTagName("root");
	
	
	if(xmlRoot.length == 0) 
		return;		
	
	
	
	var xmlQuestion = xmlRoot[0].getElementsByTagName("question");		
	
	if(xmlQuestion.length > 0) {		
		var currentQuestion=xmlQuestion[0];
		var xmlAnswers = currentQuestion.getElementsByTagName("answer");
		
		var textToSend='<div id="questionContainer" >'+GetTextFromXML(currentQuestion.getElementsByTagName("questionText")[0])+'</div>';

		for(var i=0;i<xmlAnswers.length;i++){
		
			textToSend+=outputAnswers(	xmlAnswers[i].getAttribute('id'),
							xmlAnswers[i].getAttribute('referenceNumber'),
							xmlAnswers[i].getAttribute('isCorrect'),
							GetTextFromXML(xmlAnswers[i].getElementsByTagName("answerText")[0]),
							GetTextFromXML(xmlAnswers[i].getElementsByTagName("reference")[0])
					);
		}
	
		$( "#data").html(textToSend);
	}
	
}


function outputAnswers(id, referenceNumber, isCorrect, answerText, reference) {
	
return  '<div id="ansContainer'+id+'" refNumber="'+referenceNumber+'" isCorrect="'+isCorrect+'" class="ansContainer" >'+answerText+'</div>'+
		'<div id="ref'+referenceNumber+'" style="display:none" >'+reference+'</div>';

}

