// JavaScript Document


function logDateString() {
	
	var current = new Date();
	var cdd = current.getDate();
	var cmm = current.getMonth()+1;
	var chh = current.getHours();
	var cmi = current.getMinutes();
	var css = current.getSeconds();
	var cyyyy = current.getFullYear();
	
	if(cdd<10)
		cdd='0'+cdd; 
	if(cmm<10)
		cmm='0'+cmm; 
	if(chh<10)
		chh='0'+chh;
	if(cmi<10)
		cmi='0'+cmi; 
	if(css<10)
		css='0'+css;
	
	return cdd+'/'+cmm+'/'+cyyyy+'_'+chh+'.'+cmi+'.'+css;
	
}

var MODULE_ID = 0;



function logTrack(x) {
	
	//var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/i) ? true : false );
	var track=GetParamOrDefaultIntValue("track",0);
	
	if(track==1)
		location.href = 'log:'+MODULE_ID+'_'+x+'_'+logDateString();	
	else
		console.log('log:'+MODULE_ID+'_'+x+'_'+logDateString()); 
}
