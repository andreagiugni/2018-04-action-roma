			// TOUCH-EVENTS SINGLE-FINGER SWIPE-SENSING JAVASCRIPT
			// Courtesy of PADILICIOUS.COM and MACOSXAUTOMATION.COM
			
			// this script can be used with one or more page elements to perform actions based on them being swiped with a single finger
		
			var triggerElementID = null; // this variable is used to identity the triggering element
			var fingerCount = 0;
			var startX = 0;
			var startY = 0;
			var curX = 0;
			var curY = 0;
			var deltaX = 0;
			var deltaY = 0;
			var horzDiff = 0;
			var vertDiff = 0;
			var minLength = 20; // the shortest distance the user may swipe
			var swipeLength = 0;
			var swipeAngle = null;
			var swipeDirection = null;
			
			// The 4 Touch Event Handlers
			
			// NOTE: the touchStart handler should also receive the ID of the triggering element
			// make sure its ID is passed in the event call placed in the element declaration, like:
			// <div id="picture-frame" ontouchstart="touchStart(event,'picture-frame');"  ontouchend="touchEnd(event);" ontouchmove="touchMove(event);" ontouchcancel="touchCancel(event);">
		
			function myTouchStart(event,passedName) {
				// disable the standard ability to select the touched object
				//event.preventDefault();
				// get the total number of fingers touching the screen
			//	fingerCount = event.touches.length;
				// since we're looking for a swipe (single finger) and not a gesture (multiple fingers),
				// check that only one finger was used
				//if ( fingerCount == 1 ) {
					// get the coordinates of the touch
					startX = event.touches[0].pageX;
					startY = event.touches[0].pageY;
					// store the triggering element ID
					triggerElementID = passedName;
			//	} else {
					// more than one finger touched so cancel
				//	touchCancel(event);
				//}
			}
		
			function clickStart(event,passedName) {
				
				fingerCount = 1;
					
					// get the coordinates of the touch
					startX = event.clientX;
					startY = event.clientY;
					// store the triggering element ID
					triggerElementID = passedName;
				
			}
			
			function myTouchMove(event) {
			//	event.preventDefault();
			//	if ( event.touches.length == 1 ) {
					curX = event.touches[0].pageX;
					curY = event.touches[0].pageY;
				//} else {
				//	touchCancel(event);
				//}
			}
			
			function clickMove(event) {
			//	event.preventDefault();
					curX = event.clientX;
					curY = event.clientY;
			}
			
			function myTouchEnd(event) {
			//	event.preventDefault();
				// check to see if more than one finger was used and that there is an ending coordinate
			//	if ( fingerCount == 1 && curX != 0 ) {
					// use the Distance Formula to determine the length of the swipe
				//	swipeLength = Math.round(Math.sqrt(Math.pow(curX - startX,2) + Math.pow(curY - startY,2)));
					// if the user swiped more than the minimum length, perform the appropriate action
				//	if ( swipeLength >= minLength ) {
					//	caluculateAngle();
						determineSwipeDirection();
						processingRoutine();
						myTouchCancel(event); // reset the variables
					//} else {
					//	touchCancel(event);
				//	}	
				//} else {
				//	touchCancel(event);
				//}
			}
	
			function clickEnd(event) {
			//	event.preventDefault();
				// check to see if more than one finger was used and that there is an ending coordinate
				if ( fingerCount == 1 && curX != 0 ) {
					// use the Distance Formula to determine the length of the swipe
					swipeLength = Math.round(Math.sqrt(Math.pow(curX - startX,2) + Math.pow(curY - startY,2)));
					// if the user swiped more than the minimum length, perform the appropriate action
					//if ( swipeLength >= minLength ) {
					//	caluculateAngle();
						determineSwipeDirection();
						processingRoutine();
						myTouchCancel(event); // reset the variables
					//} else {
					//	touchCancel(event);
					//}	
				} else {
					myTouchCancel(event);
				}
			}
		
			function myTouchCancel(event) {
				// reset the variables back to default values
				fingerCount = 0;
				startX = 0;
				startY = 0;
				curX = 0;
				curY = 0;
				deltaX = 0;
				deltaY = 0;
				horzDiff = 0;
				vertDiff = 0;
				swipeLength = 0;
				swipeAngle = null;
				swipeDirection = null;
				triggerElementID = null;
			}
			
			function caluculateAngle() {
				var X = startX-curX;
				var Y = curY-startY;
				var Z = Math.round(Math.sqrt(Math.pow(X,2)+Math.pow(Y,2))); //the distance - rounded - in pixels
				var r = Math.atan2(Y,X); //angle in radians (Cartesian system)
				swipeAngle = Math.round(r*180/Math.PI); //angle in degrees
				if ( swipeAngle < 0 ) { swipeAngle =  360 - Math.abs(swipeAngle); }
			}
			
			function determineSwipeDirection() {
				
				swipeDirection = '';

				var dx = curX - startX;
				if(dx > 30) 
					swipeDirection = 'right';
				else if(dx < -30) 
					swipeDirection = 'left';
				
				var dy = curY - startY;
				if(dy > 30) 
					swipeDirection = 'down';
				else if(dy < -30) 
					swipeDirection = 'up';
						
			}
			