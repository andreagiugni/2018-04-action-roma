	
function ContentRequest (xmlfile) {    
	if (!httpRequest) {
        httpRequest = CreateHTTPRequestObject ();   
    }
    
	if (httpRequest) {          	 
		var noCacheSeed = Math.round(Math.random()*100000);
		var xmlfileNoCache = xmlfile+"?uniq="+noCacheSeed;
 
        httpRequest.open ("GET", xmlfileNoCache, true);   
        httpRequest.onreadystatechange = OnContentStateChange;
        httpRequest.send (null);
    }
}


function OnContentStateChange () {
	if (httpRequest.readyState == 0 || httpRequest.readyState == 4) {
        if (IsRequestSuccessful (httpRequest)) {    
            ParseContent();
        }
        else {
            alert ("Operation failed.");
        }
    }
}


