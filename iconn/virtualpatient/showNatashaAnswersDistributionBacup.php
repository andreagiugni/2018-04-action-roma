<?php


include("connect.php");

$WHERECLAUSE = "";//""" AND sStatus='user'";

echo "<h1>Per visit</h1>";

for($i = 1; $i < 4; $i++) {


	$q = "SELECT iAnswer, count(iAnswer) as a FROM tblVirtualPatientAction WHERE fkidPatient = '2' AND sStatus = 'user' AND iVisit =  '".$i."' ".$WHERECLAUSE." GROUP BY iAnswer ORDER BY a DESC";
	
	//echo $q;
	
	$rows = $db->GetAll($q);

	echo "<table border='1' cellpadding='3' cellspacing='0'>";

	if(count($rows) > 0)
		echo "<tr><td>VISIT</td><td>NODE</td><td>ANSWER</td><td>ID</td><td>COUNT</td></tr>"; 
	
	foreach($rows as $row) {
			echo "<tr><td>".$i."</td><td>".ReturnNodeForAnswer($row['iAnswer'])."</td><td>".ReturnTextForAnswer($row['iAnswer'])."</td><td>".$row['iAnswer']."</td><td>".$row['a']."</td></tr>"; 
	}
	
	echo "</table><br>";
}

echo "<h1>All nodes</h1>";


$mainPathNode = 1;
	
for($i = 1; $i < 22; $i++) {

	$q = "SELECT iAnswer, count(iAnswer) as a FROM tblVirtualPatientAction WHERE iNode = '".$i."' ".$WHERECLAUSE." GROUP BY iAnswer ORDER BY a DESC";
	
	//echo $q;
	
	$rows = $db->GetAll($q);

	echo "<table border='1' cellpadding='3' cellspacing='0'>";

	if(count($rows) > 0)
		echo "<tr><td>NODE</td><td>NODE</td><td>ANSWER</td><td>ID</td><td>COUNT</td><td>GOING TO NODE</td></tr>"; 
	 
	 $k = 0;
	 
	foreach($rows as $row) {
			
			if($k == 0)
				$mainPathNode = $i;

			echo "<tr><td>".$i."</td><td>".ReturnNodeForAnswer($row['iAnswer'])."</td><td>".$row['iAnswer']."</td><td>".ReturnTextForAnswer($row['iAnswer'])."</td><td>".$row['a']."</td><td>".ReturnDestinationNodeForAnswer($row['iAnswer'])."</td></tr>"; 
				
			$k++;
			
	}
	
	echo "</table><br>";
}

/*
echo "<h1>Main Path</h1>";

$mainPathNode = 1;
	
for($i = 1; $i < 22; $i++) {

	$q = "SELECT iAnswer, count(iAnswer) as a FROM tblVirtualPatientAction WHERE iNode = '".$i."' GROUP BY iAnswer ORDER BY a DESC";
	
	//echo $q;
	
	$rows = $db->GetAll($q);

	echo "<table border='1' cellpadding='3' cellspacing='0'>";

	if(count($rows) > 0)
		echo "<tr><td>NODE</td><td>NODE</td><td>ANSWER</td><td>ID</td><td>COUNT</td><td>GOING TO NODE</td></tr>"; 
	 
	 $k = 0;
	 
	foreach($rows as $row) {
			
			if($k == 0)
				$mainPathNode = $i;

			if($i == $mainPathNode)
				echo "<tr><td>".$i."</td><td>".ReturnNodeForAnswer($row['iAnswer'])."</td><td>".$row['iAnswer']."</td><td>".ReturnTextForAnswer($row['iAnswer'])."</td><td>".$row['a']."</td><td>".ReturnDestinationNodeForAnswer($row['iAnswer'])."</td></tr>"; 
				
			$k++;
			
	}
	
	echo "</table><br>";
}
*/
	
function  ReturnNodeForAnswer($i) {

//return $i;

switch($i) {

// <node id="1" type="treatment">
		
	case 1:
	case 2:
	case 3:
	case 4:
	case 5:
	case 6:
	case 7:

		return 1;
		break;

	case 8:
	case 9:
	case 10:
	case 11:
	
		return 4;
		break;
	
	case 12:
	case 13:
	case 14:
	case 15:
	
		return 7;
		break;
				
	case 16:
	case 17:
	case 18:
	case 19:
	
		return 10;
		break;
				
	case 20:
	case 21:
	case 22:
	case 23:
	
		return 13;
		break;
		
	case 24:
	case 25:
	case 26:
	case 27:
	
		return 16;
		break;
		
	case 28:		
	case 29:
	case 30:
	case 31:
	
		return 19;
		break;
		
	case 32:	
	case 33:
	case 34:
	case 35:
	
		return 22;
		break;
		
	case 36:		
	case 37:
	case 38:
	case 39:
	
		return 23;
		break;
	
	case 40:
	case 41:
	case 42:
	case 43:
	
		return 24;
		break;
	
	case 44:
	case 45:
	case 46:
	
		return 25;
		break;
	
	case 47:
	case 48:
	case 49:
	
		return 26;
		break;
	
}

}
function returnTextForAnswer($i) {

//return $i;

	switch($i) {
	
		case 1: return "Spinal MRI"; break;
		case 2: return "Spinal X-Ray"; break;
		case 3: return "Electromyography"; break;
		case 4: return "The PainDetect screening tool"; break;
		case 5: return "PainDetect plus Spinal MRI"; break;
		case 6: return "PainDetect plus Spinal X-Ray"; break;
		case 7: return "PainDetect plus Electromyography"; break;
		
		//Question node 4
		case 8: return "Non-steroidal anti-inflammatory drugs (NSAIDs)"; break;
		case 9: return "Tricyclic antidepressants (TCAs)"; break;
		case 10: return "Pregabalin"; break;
		case 11: return "Gabapentin"; break;
		
		//Question node 7
		case 12: return "Non-steroidal anti-inflammatory drugs (NSAIDs)"; break;
		case 13: return "Tricyclic antidepressants (TCAs)"; break;
		case 14: return "Pregabalin"; break;
		case 15: return "Gabapentin"; break;
		
		//Question node 10
		case 16: return "Non-steroidal anti-inflammatory drugs (NSAIDs)"; break;
		case 17: return "Tricyclic antidepressants (TCAs)"; break;
		case 18: return "Pregabalin"; break;
		case 19: return "Gabapentin"; break;
		
		//Question node 13
		case 20: return "Non-steroidal anti-inflammatory drugs (NSAIDs)"; break;
		case 21: return "Tricyclic antidepressants (TCAs)"; break;
		case 22: return "Pregabalin"; break;
		case 23: return "Gabapentin"; break;
		
		//Question node 16
		case 24: return "Non-steroidal anti-inflammatory drugs (NSAIDs)"; break;
		case 25: return "Tricyclic antidepressants (TCAs)"; break;
		case 26: return "Pregabalin"; break;
		case 27: return "Gabapentin"; break;
		
		//Question node 19
		case 28: return "Non-steroidal anti-inflammatory drugs (NSAIDs)"; break;		
		case 29: return "Tricyclic antidepressants (TCAs)"; break;
		case 30: return "Pregabalin"; break;
		case 31: return "Gabapentin"; break;
		
		//Question node 22
		case 32: return "Non-steroidal anti-inflammatory drugs (NSAIDs)"; break;	
		case 33: return "Tricyclic antidepressants (TCAs)"; break;
		case 34: return "Pregabalin"; break;
		case 35: return "Gabapentin"; break;
		
		//Question node 23
		case 36: return "Change the NSAID"; break;		
		case 37: return "Add pregabalin"; break;
		case 38: return "Add gabapentin"; break;
		case 39: return "Add a TCA"; break;
		
		//Question node 24
		case 40: return "Change TCA"; break;
		case 41: return "Switch to pregabalin"; break;
		case 42: return "Switch to gabapentin"; break;
		case 43: return "Add an NSAID"; break;
		
		//Question node 25
		case 44: return "Switch to gabapentin"; break;
		case 45: return "Add an NSAID"; break;
		case 46: return "Switch to a TCA"; break;
		
		//Question node 26
		case 47: return "Switch to pregabalin"; break;
		case 48: return "Add an NSAID"; break;
		case 49: return "Switch to a TCA"; break;
		
	}
}



function ReturnDestinationNodeForAnswer($i) {

switch($i) {

		//Question node 1
		case 1: return 2; break;
		case 2: return 5; break;
		case 3: return 8; break;
		case 4: return 11; break;
		case 5: return 14; break;
		case 6: return 17; break;
		case 7: return 20; break;
		
		//Question node 4
		case 8: return 23; break;
		case 9: return 24; break;
		case 10: return 25; break;
		case 11: return 26; break;
		
		//Question node 7
		case 12: return 23; break; 
		case 13: return 24; break;
		case 14: return 25; break;
		case 15: return 26; break;
		
		//Question node 10
		case 16: return 23; break;
		case 17: return 24; break;
		case 18: return 25; break;
		case 19: return 26; break;
		
		//Question node 13
		case 20: return 23; break;
		case 21: return 24; break;
		case 22: return 25; break;
		case 23: return 26; break;
		
		//Question node 16
		case 24: return 23; break;
		case 25: return 24; break;
		case 26: return 25; break;
		case 27: return 26; break;
		
		//Question node 19
		case 28: return 23; break;		
		case 29: return 24; break;
		case 30: return 25; break;
		case 31: return 26; break;
		
		//Question node 22
		case 32: return 23; break;	
		case 33: return 24; break;
		case 34: return 25; break;
		case 35: return 26; break;
		
		//Question node 23
		case 36: return 27; break;		
		case 37: return 28; break;
		case 38: return 29; break;
		case 39: return 30; break;
		
		//Question node 24
		case 40: return 31; break;
		case 41: return 32; break;
		case 42: return 33; break;
		case 43: return 34; break;
		
		//Question node 25
		case 44: return 35; break;
		case 45: return 36; break;
		case 46: return 37; break;
		
		//Question node 26
		case 47: return 38; break;
		case 48: return 39; break;
		case 49: return 40; break;
	
}

}
?>
