



var CurrentVisit;
var NextVisitTime;
var TimeToGetBack;
var TotalVisits;

var timeoutTimer;
var thankTimer;

var secondsPerVisit = 20;

function getVisitAtCurrentTime() {
		
		var currentDate = new Date().getTime();
			
		for(var i = 0; i < visits.length; i++) {
			
			//var ora = currentDate.getTime();
			//var prima = visits[i].getTime();
			
			//var z = ora - prima;
		//	if (currentDate < visits[1]) 
			//	return 0;
			
	//		if(i < visits.length) {
				if ((currentDate > visits[i]) && (currentDate < visits[i+1])) 
					return i;
		//	}
		//	else
			//	if (currentDate > visits[i]) 
				//	return i;
		}
		
		return -1;
}
	
	function getNextVisitTime() {
		
		var v = getVisitAtCurrentTime();
		
		return visits[v + 1];
		
	}
	
	function getCurrentDate() {
		
		var currentDate = new Date();
		return formatDate(currentDate);
	}
	
	
	function getCurrentTime() {
		
		var currentDate = new Date();
		return formatTime(currentDate, false);
	}
	
	
	
	
	
	
	function updateTimeAndCheckIfItIsTimeForVisit() {
	
		var currentDate = new Date().getTime();
		
		if(CurrentVisit != -1)
			if((currentDate >= NextVisitTime))
				document.location = 'visit.html?visit='+(CurrentVisit+1)+'&device='+userID;
		
				
		document.getElementById('timeValue').innerHTML = getCurrentTime();
		
		timeoutTimer=setTimeout("updateTimeAndCheckIfItIsTimeForVisit()",1000);
	}
	
	
	function visitUpdateTimeAndCheckIfItIsTimeForNextVisit() {
	
		var currentDate = new Date().getTime();
			
		if((NextVisitTime - currentDate) < 5000)	{
			document.body.style.background =  '#ff0000';
			document.getElementById('all').style.display = 'none';
			document.getElementById('expiredID').style.display = 'block';
			document.getElementById('confirmationBoxID').style.display = 'none';
			document.getElementById('questionContainer').style.display = 'none';
		
			if(CurrentVisit != TotalVisits) 
			document.getElementById('expiredID').innerHTML = '<div class="title">Sorry</div><br />No more time to answer. Next visit will start in a few seconds. Please, wait.</div>';
			else
			document.getElementById('expiredID').innerHTML = '<div class="title">Sorry</div><br />No more time to answer.</div>';
			
			
		}
		
		if(CurrentVisit != -1)
					if((currentDate >= NextVisitTime))
						if(CurrentVisit != TotalVisits) 
							document.location = 'visit.html?visit='+(CurrentVisit+1)+'&device='+userID;
						else
							document.location = 'schedule.html'+'?device='+userID;
						
		
				
		timeoutTimer = setTimeout("visitUpdateTimeAndCheckIfItIsTimeForNextVisit()",1000);
	}
	
	function afterThanking() {
	
			document.location = 'schedule.html'+'?device='+userID;
		
	}
	
	function thanks(answer) {
		
		clearTimeout(timeoutTimer);
		
		//document.body.style.background =  '#00ff00';
		document.getElementById('all').style.display = 'none';
		document.getElementById('expiredID').style.display = 'block';
			
		document.getElementById('expiredID').innerHTML = '<div class="date">You recommended <br><br><b>'+GetAnswerText(answer)+'</b><br><br></div><div class="title">Thank you</div>';
		document.getElementById('confirmationBoxID').style.display = 'none';
		
		thankTimer =setTimeout("afterThanking()",5000);
	}
	
	function thanksAfterTest() {
		
		clearTimeout(timeoutTimer);
		
		//document.body.style.background =  '#00ff00';
		document.getElementById('all').style.display = 'none';
		document.getElementById('expiredID').style.display = 'block';
			
		document.getElementById('expiredID').innerHTML = '<div class="title">Thank you for taking the test</div>';
		document.getElementById('confirmationBoxID').style.display = 'none';
		
		thankTimer =setTimeout("afterThanking()",5000);
	}
	
	
	
	function userHasStillToDoVisit(n) {
		
		a = getVisit(n);
		
		if(a) 
			if(a[2])
				return false;
		
		return true;
	}

	var visits = new Array();
	
	function LoadVisits(xml) {
	
	var itemTags = xml.getElementsByTagName ("schedule");

		  for (var i = 0; i < itemTags.length; i++) {
			   	  
			var id = parseInt(itemTags[i].getAttribute('id'));
			visits[id] = new Date(itemTags[i].getElementsByTagName("time")[0].textContent);
		  
	  }
	  


	  
	}
	
	function CreateVisitsForTheNext6Minutes() {
		
		
		a = localStorage.getItem("visitTiming");
		if(a) {
	  		visits = a.split(",");
			return;
	 	}
		
				
				var currentDate = new Date();
				
		/*		var hours = currentDate.getHours();
  	 			var minutes = currentDate.getMinutes();
	 			var seconds = currentDate.getSeconds();
				var day = currentDate.getDate();
  				var month = currentDate.getMonth();
  				var year = currentDate.getFullYear();
	//					var dateAsString = (month+1)+"/"+day+"/"+year+" "+hours+":";
	
			*/
				
				visits[0] = new Date("1/1/2010 00:00").getTime();
				
				var currentMilliseconds = currentDate.getTime();
				currentMilliseconds += 5 * 1000;
				
				for(var i = 1; i <= 7; i++) {	
						
					//visits[i] = new Date();
					//visits[i].setTime(currentMilliseconds);
					//visits[i] = visits[i].setTime(currentMilliseconds); 
					visits[i] = currentMilliseconds; 
					
					//alert(formatTime(visits[i], false)+formatDate(visits[i], false));
					currentMilliseconds += secondsPerVisit * 1000;
					
				}
				
			
				
				SaveVisitTiming("visitTiming", visits);	
	
	}
	
	
test = GetParamOrDefaultIntValue('test', 0);
	
	if(test) {
		localStorage.removeItem("visitTiming");
		localStorage.removeItem("visit_1");
		localStorage.removeItem("visit_2");
		localStorage.removeItem("visit_3");
		localStorage.removeItem("visit_4");
		localStorage.removeItem("visit_5");
		localStorage.removeItem("visit_6");
		
	
	}
	else {
		var a = localStorage.getItem("visitTiming");
		if(a) 
			test = 1;
	}
	
	if(!test)
				LoadVisits(xmlDoc);
			else {
				
				CreateVisitsForTheNext6Minutes();
			}
		
	TotalVisits = visits.length - 2;	
	CurrentVisit = getVisitAtCurrentTime();
	NextVisitTime = getNextVisitTime();
	NextVisitTimeHuman = new Date();
	NextVisitTimeHuman.setTime(NextVisitTime);
	TimeToGetBack = NextVisitTime - 3 * 1000;	