

function setVisit(user, num, node, answer, status) {
	
	 var vis = new Array();
	 
	 vis[0] = user;
	 vis[1] = node;
	 vis[2] = answer;
	 vis[3] = status;
	 vis[4] = "tbs";
	 
	 localStorage.setItem("visit_"+num, vis);	
}

function markVisitAsRead(user, num) {
	
	 var vis = getVisit(num);
	 
	 vis[4] = "sent";
	 
	 localStorage.setItem("visit_"+num, vis);	
}

function getVisit(num) {
 
	 a = localStorage.getItem("visit_"+num);
	 if(a) {
	  	vis = a.split(",")
		return vis;
	 }
	return null;
	
}

function SaveVisitTiming(name, arr) {
	
	var serial = "";
	
	for(var i = 0; i < arr.length; i++) {
		
		serial += arr[i];
		
		if(i != arr.length - 1)
			serial += ",";
	}
	
	localStorage.setItem(name, serial);	
		
}



//setVisit(1, 1, 3, 2, 16);
//setVisit(1, 2, 1, null, 16);


/*
var db = openDatabase("VirtualPatient", "1.0", "Virtual Patient", 200000);
var dataset;
 
 	
function createTable() { 
	db.transaction(function(tx) {
          tx.executeSql("CREATE TABLE IF NOT EXISTS VirtualPatientVisit (id INTEGER PRIMARY KEY AUTOINCREMENT, userId INTEGER, visit INTEGER, node INTEGER, answer INTEGER, pasi INTEGER)");
     });
}


var debug = false;

function insertVisit(user, visit, node, answer, pasi) {
        
		var query = "DELETE FROM VirtualPatientVisit WHERE userId = ? AND visit = ?";
		
		db.transaction(function(tx) {
			
				tx.executeSql(query, 
				
				[user, visit],
		
				function onSuccess(tx, result) {
					 if(debug)
					 	console.log(query + " OK");
				},
				
				function onError(tx, error) {
					 if(debug)
					 	alert(query + " FAILED WITH: " +error.message);
					 	
				}
				
			);
		
		});
		
		db.transaction(function(tx) {
      	   
		   var query = "INSERT INTO VirtualPatientVisit (userId, visit, node, answer, pasi) VALUES (?, ?, ?, ?, ?)";
		   
		   tx.executeSql(
		   
		   		query, 
		   		
				[user, visit, node, answer, pasi],
		   
		   		function onSuccess(tx, result) {
					 if(debug)
					 	console.log(query + " OK");
				},
				
				function onError(tx, error) {
					 if(debug)
					 	alert(query + " FAILED WITH: " +error.message);
					 	
				}
			
		   );
		   
        });
		


}

var userVisits = new Array;

function GetUserVisits(user, htmlToFill, xml) {
	
	db.transaction(function(tx) {
		 
		 tx.executeSql("SELECT * FROM VirtualPatientVisit WHERE userId = ? ", [user], 
		 
		 function(tx, result) {
			 
				dataset = result.rows;
				
				htmlToFill.innerHTML = "<br>";
					
            	for (var i = 0, item = null; i < dataset.length; i++) {
					
					item = dataset.item(i);  
					
			
					htmlToFill.innerHTML  +=      
					GetAnswerTextForNodeAndIndex(xml, item['visit'],  item['node'], item['answer']);
					ShowPageBody(xml, item['node'], item['node']+"bb", true);
					ShowPasi(xml, item['node'], item['node']+"pasi", true);
					
				}
					       	
          },
		  
		  function onError(tx, error) {
   				 alert(error.message);
			}
		  
		  
		  );
      }
   );
} 


function myTransactionErrorCallback(error) {

     alert('Oops. Error was '+error.message+' (Code '+error.code+')');
}

createTable();

insertVisit(1,1,1,2,0);
insertVisit(1,2,2,1,0);




function onError(tx, error) {
    alert(error.message);
}
 
  
 
 				
/*			
var selectAllStatement = "SELECT * FROM Contacts";
 var insertStatement = "INSERT INTO Contacts (firstName, lastName, phone) VALUES (?, ?, ?)";
 var updateStatement = "UPDATE Contacts SET firstName = ?, lastName = ?, phone = ? WHERE id = ?";
 var deleteStatement = "DELETE FROM Contacts WHERE id=?";
 var dropStatement = "DROP TABLE Contacts";
 
 
 
       
      function showRecords() {

        results.innerHTML = '';
        db.transaction(function(tx) {
          tx.executeSql(selectAllStatement, [], function(tx, result) {
dataset = result.rows;
            for (var i = 0, item = null; i < dataset.length; i++) {
              item = dataset.item(i);
              results.innerHTML += 
                  '<li>' + item['lastName'] + ' , ' + item['firstName'] + ' <a href="#" onclick="loadRecord('+i+')">edit</a>  ' +  
 '<a href="#" onclick="deleteRecord('+item['id']+')">delete</a></li>';
}
          });
        });
      }
      function createTable() { 
        db.transaction(function(tx) {
          tx.executeSql(createStatement, [], showRecords, onError);
        });
      }
       
      function insertRecord() {
        db.transaction(function(tx) {
          tx.executeSql(insertStatement, [firstName.value, lastName.value, phone.value], loadAndReset, onError);
        });
      }
 function loadRecord(i) {
var item = dataset.item(i); 
        firstName.value = item['firstName'];
lastName.value = item['lastName'];
phone.value = item['phone'];
id.value = item['id']; 
      }
 
      function updateRecord() {
        db.transaction(function(tx) {
          tx.executeSql(updateStatement, [firstName.value, lastName.value, phone.value, id.value], loadAndReset, onError);
        }); 
      }

      function deleteRecord(id) {
        db.transaction(function(tx) {
          tx.executeSql(deleteStatement, [id], showRecords, onError);
        });
resetForm();
      }

      function dropTable() {
        db.transaction(function(tx) {
          tx.executeSql(dropStatement, [], showRecords, onError);
        });

resetForm();
      }


function loadAndReset(){
099
 resetForm();
100
 showRecords();
101
 }
102
 
103
 function resetForm(){
104
firstName.value = '';
105
lastName.value = '';
106
phone.value = '';
107
id.value = ''; 
108
 }
109
    </script>
110
</html>​
*/

/*		 db.transaction(function(tx) {
		 tx.executeSql("SELECT * FROM VirtualPatientVisit WHERE userId = ? ", [user], 
		 
		 function(tx, result) {
				dataset = result.rows;
            	for (var i = 0, item = null; i < dataset.length; i++) {
              		
				item = dataset.item(i);
              	
				
				results.innerHTML += 
                  '<li>' + item['lastName'] + ' , ' + item['firstName'] + ' <a href="#" onclick="loadRecord('+i+')">edit</a>  ' +  
 '<a href="#" onclick="deleteRecord('+item['id']+')">delete</a></li>';
				}
          });
         });
*/
